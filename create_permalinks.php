<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

define('PHPWG_ROOT_PATH','./');
include_once(PHPWG_ROOT_PATH . 'include/common.inc.php');
include_once(PHPWG_ROOT_PATH . 'include/functions_notification.inc.php');
include_once(PHPWG_ROOT_PATH . 'include/functions_category.inc.php');

define('WEBSITE_SUFFIX', ' - Foto-Stendel.de - Photography - Stefan und Sabine Stendel - Hamburg');
define('META_CAT_TABLE', 'meta_cat');

getEmptyCatDescription();
echo "<pre>";
echo createPermalinks() . ' Permalinks neu erstellt.' . "\n";
echo createMetadata() . ' Metadaten neu erstellt.' . "\n";
echo createAlbumTitle() . ' Album Titel neu erstellt.' . "\n";
echo createPhotoTitle() . ' Photo Titel neu erstellt.' . "\n";
echo "<a href='create_sitemap.php'>create Sitemap</a>" . "\n";
echo "</pre>";


/**
 * Get and print the empty Category Description
 */
function getEmptyCatDescription()
{
    $query = "SELECT id, name, permalink FROM " . CATEGORIES_TABLE . " WHERE comment IS NULL";
    $result = pwg_query($query);
    echo "<pre>";
    if($result->num_rows != 0) {
        echo "Folgende [" . $result->num_rows . "] Kategorien haben keine Beschreibung.\n<br />";
        while ($row = pwg_db_fetch_assoc($result)) {
            echo "<a href='/admin.php?page=album-" . $row['id'] . "' target='_blank'>" . $row['id'] . " - " . $row['name'] . "</a>\n";
        }
    } else {
        echo 'Keine leeren Kategoriebeschreibungen.';
    }
    echo "</pre>";
}


/**
 * Create the Permalinks for Albums
 */
function createPermalinks()
{
    $parentCatName = '';
    $query = "UPDATE " . CATEGORIES_TABLE . " SET permalink = NULL ";
    pwg_query($query);

    $readQuery = "SELECT id, name, uppercats FROM " . CATEGORIES_TABLE . " WHERE permalink IS NULL";
    $readResult = pwg_query($readQuery);

    while ($cat = pwg_db_fetch_assoc($readResult)) {
        if (substr_count($cat['uppercats'], ',') >= 1) {
            $parentCatId = explode(',', $cat['uppercats']);

            foreach ($parentCatId as $catId) {
                $parentCatQuery = "SELECT name FROM " . CATEGORIES_TABLE . " WHERE id = '" . $catId . "'";
                $parentCatResult = pwg_query($parentCatQuery);
                $parentCat = pwg_db_fetch_assoc($parentCatResult);
                $parentCatName .= mb_strtolower($parentCat['name']) . "/";
            }
        } else {
            $parentCatName = $cat['name'] . '/';
        }
        $newPermalink = substr(preg_replace('/ /', '_', mb_strtolower($parentCatName)), 0, -1) . '.html';

        $query = "UPDATE " . CATEGORIES_TABLE . " SET permalink = '" . $newPermalink . "' WHERE id = " . $cat['id'] . ";";

        if (!pwg_query($query)) {
            echo 'FEHLER';
        }
        $parentCatName = '';
    }
    return $readResult->num_rows;
}


/**
 * Create the Metadata for Categories from Categorie names
 */
function createMetadata()
{
    $query = "TRUNCATE TABLE " . META_CAT_TABLE;
    pwg_query($query);

    $query = "SELECT id, name, comment FROM " . CATEGORIES_TABLE . " WHERE comment IS NOT NULL";
    $result = pwg_query($query);

    while ($cat = pwg_db_fetch_assoc($result)) {
        $query = "INSERT INTO " . META_CAT_TABLE . " (id, metaKeycat, metadescat) VALUES ('" . $cat['id'] . "', '', '" . trim(strip_tags($cat['comment'])) . "');";
        if (!pwg_query($query)) {
            echo 'FEHLER beim schreiben in ' . META_CAT_TABLE;
        }
    }
    return $result->num_rows;
}


/**
 * Create the Album Titles from Category name
 */
function createAlbumTitle()
{
    $query = "TRUNCATE TABLE " . TITLE_ALBUM_TABLE;
    pwg_query($query);

    $query = "SELECT id, name, comment FROM " . CATEGORIES_TABLE . " WHERE comment IS NOT NULL";
    $result = pwg_query($query);

    while ($cat = pwg_db_fetch_assoc($result)) {
        $query = "INSERT INTO " . TITLE_ALBUM_TABLE . " (id, title) VALUES ('" . $cat['id'] . "', '" . $cat['name'] . " " . WEBSITE_SUFFIX . "');";
        if (!pwg_query($query)) {
            echo 'FEHLER beim schreiben in ' . TITLE_ALBUM_TABLE;
        }
    }
    return $result->num_rows;
}


/**
 * Create the Image Titles from Category of the Image and the Image name
 */
function createPhotoTitle()
{
    $query = "TRUNCATE TABLE " . TITLE_PHOTO_TABLE;
    pwg_query($query);

    $query = 'SELECT
      i.id,
      i.name,
      i.file,
      DATE(i.lastmodified) AS lastmodified,
      concat(replace(file, substring(file,-4), "-la"),substring(file,-4)) AS file,
      c.dir,
      c.uppercats
    FROM
      images i
    INNER JOIN
      image_category ic ON (i.id = ic.image_id)
    JOIN
      categories c ON (ic.category_id = c.id)';
    $result = pwg_query($query);
    $parentCatName = '';
    while ($row = pwg_db_fetch_assoc($result)) {
        if(substr_count($row['uppercats'], ',') >= 1) {
            $parentCatId = explode(',', $row['uppercats']);

            foreach($parentCatId as $catId) {
                $parentCatQuery = "SELECT name FROM " . CATEGORIES_TABLE . " WHERE id = '" . $catId . "'";
                $parentCatResult = pwg_query($parentCatQuery);
                $parentCat = pwg_db_fetch_assoc($parentCatResult);
                $parentCatName .= $parentCat['name'] . "/";
            }
        } else {
            $parentCatName = $row['name'] . '/';
        }
        $title = preg_replace('[/]', '|', $parentCatName) . $row['name'] . ' [' . $row['file'] . '] - Foto-Stendel.de - Photography - Stefan und Sabine Stendel - Hamburg';
        $query = "INSERT INTO " . TITLE_PHOTO_TABLE . " (id, title) VALUES ('" . $row['id'] . "', '" . $title . "')";
        if (!pwg_query($query)) {
            echo 'FEHLER beim schreiben in ' . TITLE_PHOTO_TABLE;
        }
        $parentCatName = '';
    }
    return $result->num_rows;
}


/**
 * @param $string
 * @return string
 */
function umlautepas($string){
    $upas = Array("ä" => "ae", "ü" => "ue", "ö" => "oe", "Ä" => "Ae", "Ü" => "Ue", "Ö" => "Oe", "ß" => "ss");
    return strtr($string, $upas);
}
