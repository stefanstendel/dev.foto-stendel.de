        <!-- Start of footer.tpl -->
        <div class="copyright container">
            <div class="text-center">
{if isset($debug.TIME)}
                {'Page generated in'|@translate} {$debug.TIME} ({$debug.NB_QUERIES} {'SQL queries in'|@translate} {$debug.SQL_TIME}) -
{/if}
                <div class="stendel-footer">
                  <div id="footer-1"><b>Foto-Stendel.de</b><br />Sabine & Stefan Stendel<br />Riehlstraße 36<br/>D-21033 Hamburg</div>
                  <div id="footer-2"><br />Tel: +49(40) 412 83 911 [ab 18:00 Uhr]<br/ >Mobil: +49(176) 481 83 911<br />Mail: <a href="maito:post@foto-stendel.de">post@foto-stendel.de</a></div>
                <div id="footer-3"><br />
                  <ul id="css3columnlist">
                    <li><a href="{$ROOT_URL}index/page/ueber_uns.html">Über uns</a></li>
                    <li><a href="{$ROOT_URL}index/page/agb.html">AGB</a></li>
                    <li><a href="{$ROOT_URL}index/page/impressum.html">Impressum</a></li>
                    <li><a href="{$ROOT_URL}index/page/models_gesucht.html">Models gesucht!</a></li>
                    <li><a href="{$ROOT_URL}index/page/shooting_gesucht.html">Shootings gesucht?</a></li>
                    <li><a href="{$ROOT_URL}index/page/preise.html">Preise</a></li>
                    <li><a href="{$ROOT_URL}index/page/links.html">Links</a></li>
                    <li><a href="{$ROOT_URL}index/page/downloads.html">Downloads</a></li>
                    <li><a href="{$ROOT_URL}index/page/kontakt.html">Kontakt</a></li>
                  </ul>
                </div>
                  <div id="footer-4"><br /><br />Site powered by <a href="http://de.piwigo.org/" target="_blank">Piwigo</a><br />© in 2016 by Foto-Stendel.de</div>
                </div>
                {*{'Powered by'|@translate}	<a href="{$PHPWG_URL}" class="Piwigo">Piwigo</a>*}
{$VERSION}
{*{if isset($CONTACT_MAIL)}*}
                {*| <a href="mailto:{$CONTACT_MAIL}?subject={'A comment on your site'|@translate|@escape:url}">{'Contact webmaster'|@translate}</a>*}
{*{/if}*}
{if isset($TOGGLE_MOBILE_THEME_URL)}
                | {'View in'|@translate} : <a href="{$TOGGLE_MOBILE_THEME_URL}">{'Mobile'|@translate}</a> | <b>{'Desktop'|@translate}</b>
{/if}

{get_combined_scripts load='footer'}

{if isset($footer_elements)}
{foreach from=$footer_elements item=v}
{$v}
{/foreach}
{/if}
            </div>
        </div>
{if isset($debug.QUERIES_LIST)}
        <div id="debug">
{$debug.QUERIES_LIST}
        </div>
{/if}
    </div>
{if $theme_config_extra->photoswipe && ($BODY_ID == "thePicturePage" || !empty($THUMBNAILS))}
{include file='_photoswipe_div.tpl'}
{/if}
</body>
</html>
