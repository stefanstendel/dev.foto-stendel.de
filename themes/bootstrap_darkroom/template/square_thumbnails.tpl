{if !empty($thumbnails)}
    {footer_script}
        var error_icon = "{$ROOT_URL}{$themeconf.icon_dir}/errors_small.png", max_requests = {$maxRequests};
    {/footer_script}

    {assign var=width value=$derivative_params->sizing->ideal_size[0]}
    {assign var=height value=$derivative_params->sizing->ideal_size[1]}
    {assign var=width value=180}
    {assign var=height value=180}
    {assign var=rwidth value=$width}
    {assign var=rheight value=$height}

    {define_derivative name='derivative_params' width=$width height=$height crop=true}
    <style>
        .placeholder-{$rwidth} {
            width: {$rwidth}px;
            height: {$rheight}px;
            background: url({$ROOT_URL}{$themeconf.icon_dir}/img_small.png) no-repeat center;
        }
    </style>
    {assign var=idx value=0}
    {foreach from=$thumbnails item=thumbnail}
        {assign var=derivative value=$pwg->derivative($derivative_params, $thumbnail.src_image)}

        {include file="grid_classes.tpl" width=$width height=$height}
        {*<p>{$derivative->get_url()->compute_final_size('SQ')}</p>*}



    {/foreach}
{/if}
