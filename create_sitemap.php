<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

define('PHPWG_ROOT_PATH', './');
define('DOCUMENT_ROOT', '/var/www/vhosts/foto-stendel.de/httpdocs/');

include_once(PHPWG_ROOT_PATH . 'include/common.inc.php');
include_once(PHPWG_ROOT_PATH . 'include/functions_notification.inc.php');
include_once(PHPWG_ROOT_PATH.'include/functions_picture.inc.php');
include_once(PHPWG_ROOT_PATH.'include/functions_category.inc.php');

define('BASE_URL', 'http://www.foto-stendel.de/');

define('PAGE_URL','index/page/');
define('PAGE_SITEMAP_FREQ', 'daily');
define('PAGE_SITEMAP_PRIO', '0.8');

define('CAT_URL','index/category/');
define('CAT_SITEMAP_FREQ', 'daily');
define('CAT_SITEMAP_PRIO', '0.8');

define('TAGS_URL','index/tags/');
define('TAGS_SITEMAP_FREQ', 'daily');
define('TAGS_SITEMAP_PRIO', '0.8');

define('IMG_URL', '_data/i/galleries/');
define('IMG_SITEMAP_FREQ', 'daily');
define('IMG_SITEMAP_PRIO', '0.8');

$private_cats = array('1', '188', '192', '262');

$content = '<?xml version="1.0" encoding="UTF-8"?>
 <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

$pf = fopen ("sitemap.xml", "w");

/**
 * Create Additional Pages
 */
$addPageCount = 1;
$query = "SELECT permalink, DATE(lastmodified) AS lastmodified FROM " . ADD_PAGES_TABLE . " WHERE pos > 0";
$result = pwg_query($query);
$content .= "
<!-- BEGIN ADDITIONAL PAGES -->";
while ($row = pwg_db_fetch_assoc($result)) {
    $content .= "
    <url>
        <loc>" . BASE_URL . PAGE_URL . $row['permalink'] . "</loc>
        <lastmod>" . $row['lastmodified'] . "</lastmod>
        <changefreq>" . PAGE_SITEMAP_FREQ . "</changefreq>
        <priority>" . PAGE_SITEMAP_PRIO . "</priority>
    </url>";
    $addPageCount++;
}

/**
 * Create Categories URLS
 */
$categoriesCount = 1;
$query = "SELECT id, permalink, DATE(lastmodified) AS lastmodified FROM " . CATEGORIES_TABLE . " LEFT JOIN user_access ON categories.id = user_access.cat_id WHERE user_access.user_id = 2 AND categories.visible = true ORDER BY categories.id ";

$result = pwg_query($query);
$content .= "
<!-- BEGIN CATEGORIES -->";
while ($row = pwg_db_fetch_assoc($result)) {
    $content .= "
    <url>
        <loc>" . BASE_URL . CAT_URL . $row['permalink'] . "</loc>
        <lastmod>" . $row['lastmodified'] . "</lastmod>
        <changefreq>" . CAT_SITEMAP_FREQ . "</changefreq>
        <priority>" . CAT_SITEMAP_PRIO . "</priority>
    </url>";
    $categoriesCount++;
}

/**
 * Create Tags URLS
 */
$tagsCount = 1;
$query = "SELECT name, url_name, DATE(lastmodified) AS lastmodified FROM " . TAGS_TABLE . " ORDER BY url_name";
$result = pwg_query($query);
$content .= "
<!-- BEGIN TAGS -->";
while ($row = pwg_db_fetch_assoc($result)) {
    $content .= "
    <url>
        <loc>" . BASE_URL . TAGS_URL . preg_replace('[ ]', '_', mb_strtolower($row['name'])) . "</loc>
        <lastmod>" . $row['lastmodified'] . "</lastmod>
        <changefreq>" . TAGS_SITEMAP_FREQ . "</changefreq>
        <priority>" . TAGS_SITEMAP_PRIO . "</priority>
    </url>";
    $tagsCount++;
}

/**
 * Create Photo URLS
 */
$imagesCount = 1;
$query = '
SELECT
	i.id,
	IF(i.name IS NULL, i.file, i.name) AS label,
	i.file,
	i.path,
    DATE(i.lastmodified) AS lastmodified,
    i.name,
	i.comment,
	c.id AS cat_id,
	c.name AS cat_name,
	c.permalink AS cat_permalink
FROM images i
INNER JOIN image_category AS ic ON i.id = ic.image_id
JOIN categories AS c ON ic.category_id = c.id
LEFT JOIN user_access ON c.id = user_access.cat_id WHERE user_access.user_id = 2
'.get_sql_condition_FandF
    (
        array
        (
            'forbidden_categories' => 'category_id',
            'visible_categories' => 'category_id',
            'visible_images' => 'id'
        ),
        'WHERE'
    ).'
;';

$image_infos = query2array($query, 'id');

$content .= "
<!-- BEGIN IMAGES -->";
foreach ($image_infos as $row) {

    $imagePath = substr(substr($row['path'], 0, -4) . '-la' . substr($row['path'], -4, 4), 2);
    $imageUrl = make_picture_url(
                  array(
                      'category' => array(
                        'id' => $row['cat_id'],
                        'name' => $row['cat_name'],
                        'permalink' => $row['cat_permalink'],
                        ),
                      'image_id' => $row['id'],
                    'image_file' => $row['file'],
                  )
                );

    if (file_exists(DOCUMENT_ROOT . '_data/i/' . $imagePath)) {
        $content .= "
    <url>
        <loc>" . BASE_URL . $imageUrl . "</loc>
        <image:image>
            <image:loc>" . BASE_URL . "i/" . $imagePath . "</image:loc>
            <image:caption>" . umlautepas(trim(strip_tags($row['comment']))) . "</image:caption>
            <image:title>" . trim(strip_tags($row['name'])) . "</image:title>
            <image:license>http://www.foto-stendel.de/index/page/agb.html</image:license>
        </image:image>
        <lastmod>" . $row['lastmodified'] . "</lastmod>
        <changefreq>" . IMG_SITEMAP_FREQ . "</changefreq>
        <priority>" . IMG_SITEMAP_PRIO . "</priority>
    </url>";
        $imagesCount++;
    }
}
$content .= "
</urlset>\n";

fwrite ($pf, $content);
fclose ($pf);

echo "<pre>";
echo "Additional Pages: " . $addPageCount . "\n";
echo "Categories:       " . $categoriesCount . "\n";
echo "Tags:             " . $tagsCount . "\n";
echo "Images:           " . $imagesCount . "\n";
echo "Total URLS:       " . ($addPageCount + $categoriesCount + $tagsCount + $imagesCount) . "\n";
echo "</pre>";
echo "<a href='create_permalinks.php'>create Permalinks</a>";


function umlautepas($string){
    $upas = Array("ä" => "ae", "ü" => "ue", "ö" => "oe", "Ä" => "Ae", "Ü" => "Ue", "Ö" => "Oe", "ß" => "ss", "&" => "und");
    return strtr($string, $upas);
}
