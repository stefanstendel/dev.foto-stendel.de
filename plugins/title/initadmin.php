<?php
// +-----------------------------------------------------------------------+
// | Title plugin for piwigo                                               |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2011 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
//load_language('plugin.lang', TITLE_PATH);

//Add link menu
add_event_handler('get_admin_plugin_menu_links', 'title_admin_menu');
function title_admin_menu($menu)
{
  array_push($menu, array(
	'NAME' => l10n('Page title'),
    'URL' => get_admin_plugin_menu_link(TITLE_PATH . 'admin.php')));
  return $menu;
}


//add prefiltre photo
add_event_handler('loc_begin_admin', 'titlePadminf',60);
add_event_handler('loc_begin_admin_page', 'titlePadminA',60);

function titlePadminf()
	{
	global $template;
	$template->set_prefilter('picture_modify', 'titlePadminfT');
	}

function titlePadminfT($content, &$smarty)
	{
  $search = '#<p style="margin:40px 0 0 0">#';
  
  $replacement = '
    <p>
      <strong>{\'title_photo\'|@translate}</strong>
      <br>
      <textarea rows="4" cols="80" {if $useED==1}placeholder="{\'Use Extended Description tags...\'|@translate}"{/if} name="insertitleP" id="insertitleP" class="insertitleP">{$titleCONTENT}</textarea>
    </p>
	
<p style="margin:40px 0 0 0">';

  return preg_replace($search, $replacement, $content);
	}
 
function titlePadminA(){
  if (isset($_GET['image_id'])){
	global $template, $prefixeTable;
	$query = 'select id,title FROM ' . TITLE_PHOTO_TABLE . ' WHERE id = '.$_GET['image_id'].';';
	$result = pwg_query($query);
	$row = pwg_db_fetch_assoc($result);
    $titleP=$row['title'];
    $PAED = pwg_db_fetch_assoc(pwg_query("SELECT state FROM " . PLUGINS_TABLE . " WHERE id = 'ExtendedDescription';"));
	if($PAED['state'] == 'active'){
	  $template->assign('useED',1);
    }else{
      $template->assign('useED',0);
    }
    $template->assign(
      array(
        'titleCONTENT' => $titleP,
    ));
  }
  if (isset($_POST['insertitleP'])){
	$query = 'DELETE FROM ' . TITLE_PHOTO_TABLE . ' WHERE id = '.$_GET['image_id'].';';
	$result = pwg_query($query);
	$q = 'INSERT INTO ' . $prefixeTable . 'title_photo(id,title)VALUES ('.$_GET['image_id'].',"'.$_POST['insertitleP'].'");';
	pwg_query($q);
	$template->assign(
	  array(
		'titleCONTENT' => $_POST['insertitleP'],
	));
  }
}
	
//add prefiltre album
add_event_handler('loc_begin_admin', 'titleAadminf', 60);
add_event_handler('loc_begin_admin_page', 'titleAadminA', 60);

function titleAadminf()
 {
	global $template;
	$template->set_prefilter('album_properties', 'titleAadminfT');
 }

function titleAadminfT($content, &$smarty)
 {
  $search = '#<p style="margin:0">#';
  
  $replacement = '
    <p>
      <strong>{\'title_album\'|@translate}</strong>
      <br>
      <textarea rows="4" cols="80" {if $useED==1}placeholder="{\'Use Extended Description tags...\'|@translate}"{/if} name="insertitleA" id="insertitleA" class="insertitleA">{$titleCONTENT}</textarea>
    </p>	
	
	
<p style="margin:0">';

  return preg_replace($search, $replacement, $content);
 }

function titleAadminA(){ 
  if (isset($_GET['cat_id'])){
 	global $template, $prefixeTable;
	$query = 'select id,title FROM ' . TITLE_ALBUM_TABLE . ' WHERE id = '.$_GET['cat_id'].';';
	$result = pwg_query($query);
	$row = pwg_db_fetch_assoc($result);
	$titleA=$row['title'];
    $PAED = pwg_db_fetch_assoc(pwg_query("SELECT state FROM " . PLUGINS_TABLE . " WHERE id = 'ExtendedDescription';"));
	if($PAED['state'] == 'active'){
	  $template->assign('useED',1);
    }else{
      $template->assign('useED',0);
    }
	$template->assign(
      array(
        'titleCONTENT' => $titleA,
    ));
  }
  if (isset($_POST['insertitleA'])){
	$query = 'DELETE FROM ' . TITLE_ALBUM_TABLE . ' WHERE id = '.$_GET['cat_id'].';';
	$result = pwg_query($query);
	$q = 'INSERT INTO ' . $prefixeTable . 'title_album(id,title)VALUES ('.$_GET['cat_id'].',"'.$_POST['insertitleA'].'");';
    pwg_query($q);
	$template->assign(
      array(
        'titleCONTENT' => $_POST['insertitleA'],
    ));
  }
}

?>