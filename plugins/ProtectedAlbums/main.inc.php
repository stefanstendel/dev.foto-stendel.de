<?php
/*
Plugin Name: Protected Albums
Version: 0.5.b
Description: Password Protect an Album
Plugin URI: http://piwigo.org/ext/extension_view.php?eid=597
Author: unclvito
Author URI: http://www.photographicmoments.net/ 
Co-Author: leonardo.g
Co-Author URI: http://www.leobox.it/
*/

/* History:  PROTECTALBUM_PATH.'Changelog.txt.php' */

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

global $prefixeTable;

// Display messages on index page
// ------------------------------
add_event_handler('init', 'PA_InitPage');

function PA_InitPage()
{
	global $prefixeTable;
	define('PROTECTALBUM_TABLE', $prefixeTable.'protectalbum');
	if (!defined('PROTECTALBUM_PATH')) define('PROTECTALBUM_PATH' , PHPWG_PLUGINS_PATH.basename(dirname(__FILE__)).'/');
	
	include_once (PROTECTALBUM_PATH.'include/functions.inc.php');
		
	load_language('plugin.lang', PROTECTALBUM_PATH);
	
	
	$pa_include_file = dirname(__FILE__)."/".script_basename().'.inc.php';
	if (file_exists($pa_include_file))
	  include_once($pa_include_file);
}
?>