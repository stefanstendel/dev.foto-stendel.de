<?php
global $conf, $template, $page;

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
if (!defined('PROTECTALBUM_PATH')) define('PROTECTALBUM_PATH' , PHPWG_PLUGINS_PATH.basename(dirname(__FILE__)).'/');

load_language('plugin.lang', PROTECTALBUM_PATH);

$conf['ProtectAlbum'] = explode(',', $conf['ProtectAlbum']);

$alternativeThumb=(isset($_POST['alternativeThumb'])?$_POST['alternativeThumb']:'locked3.jpg');


if (isset($_POST['Set']))
{
	// set a new thumbnails for all protected album
  pwg_query("UPDATE ".PROTECTALBUM_TABLE." SET `hideThumb` ='true' , `alternativeThumb`='".$alternativeThumb."' WHERE enabled");
  $page['infos'][] = l10n('Information data registered in database');
}
else if (isset($_POST['Reset']))
{
	// set the dafault thumbnails for all protected album
  pwg_query("UPDATE ".PROTECTALBUM_TABLE." SET `hideThumb` ='false' , `alternativeThumb`='".$alternativeThumb."' WHERE enabled");
  $page['infos'][] = l10n('Information data registered in database');
}

// the prefilter changes, we must delete compiled templatess
$template->delete_compiled_templates();

/*
if (isset($_POST['submit'])){
  
  conf_update_param('ProtectAlbum', implode(',', $conf['ProtectAlbum']));
  array_push($page['infos'], l10n('Information data registered in database'));
  
  // the prefilter changes, we must delete compiled templatess
  $template->delete_compiled_templates();
}
*/

// +-----------------------------------------------------------------------+
// |                      Getting plugin version                           |
// +-----------------------------------------------------------------------+
$plugin =  PROTECTALBUM_Infos(PROTECTALBUM_PATH);
$version = $plugin['version']; 

$template->assign(array(
  'PROTECTALBUM_PATH' => PROTECTALBUM_PATH,
  'PA_VERSION'    => $version
));

$template->set_filename('protectalbum_content', dirname(__FILE__).'/template/admin.tpl');
$template->assign_var_from_handle('ADMIN_CONTENT', 'protectalbum_content');
?>