<?php
global $prefixeTable;
if (!defined('PROTECTALBUM_PATH')) define('PROTECTALBUM_PATH' , PHPWG_PLUGINS_PATH.basename(dirname(__FILE__)).'/');
if (!defined('PROTECTALBUM_TABLE')) define('PROTECTALBUM_TABLE', $prefixeTable.'protectalbum');

include_once (PROTECTALBUM_PATH.'include/functions.inc.php');

load_language('plugin.lang', PROTECTALBUM_PATH);

function plugin_install()
{ 
	//pwg_query('DELETE FROM '.CONFIG_TABLE.' WHERE param="ProtectAlbum" LIMIT 1;');
  pwg_query('INSERT IGNORE INTO '.CONFIG_TABLE.' (param,value,comment) VALUES ("ProtectAlbum","medium","ProtectAlbum config");');
  
  
  pwg_query('CREATE TABLE IF NOT EXISTS `'.PROTECTALBUM_TABLE.'` (
	    `cat_id` smallint(6) NOT NULL,
	    `enabled` enum(\'true\',\'false\') NOT NULL DEFAULT \'true\',
	    `question` varchar(120) NOT NULL,
	    `password` varchar(32) NOT NULL,
	    `hideThumb` enum(\'true\',\'false\') NOT NULL DEFAULT \'false\',
	    `alternativeThumb` varchar(255) NOT NULL DEFAULT \'locked4.jpg\',
	    PRIMARY KEY (`cat_id`)
	    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;');
	
	//check if field exixts and
	//updates the database from previous versions
  // ------------------------------------------
  //add a NULL record to have min 1 record!!
  pwg_query("REPLACE INTO ".PROTECTALBUM_TABLE." (`cat_id`, `enabled`, `question`, `password`) VALUES (32768, 'true', '', 'none')");
  $query='SELECT * FROM `'.PROTECTALBUM_TABLE.'` LIMIT 1';
  $result = pwg_query($query);
  $mycol = pwg_db_fetch_assoc($result);
  if(!isset($mycol['hideThumb']))
  {
  	 pwg_query('ALTER TABLE `'.PROTECTALBUM_TABLE.'` 
  		ADD COLUMN `hideThumb` enum(\'true\',\'false\') NOT NULL DEFAULT \'false\',
  		ADD COLUMN `alternativeThumb` varchar(255) NOT NULL DEFAULT \'locked4.jpg\' ');
  }    
  //delete the NULL record 
  pwg_query("DELETE FROM ".PROTECTALBUM_TABLE." WHERE `cat_id`= 32768");
}

// ***********************************************************************
function plugin_activate()
{
  //change_pa_conf('activated', '1');
  plugin_install();
}

// ***********************************************************************
function plugin_deactivate()
{
  //change_pa_conf('activated', '0');
  pwg_query('DELETE FROM '.CONFIG_TABLE.' WHERE param="ProtectAlbum" LIMIT 1;');
} 

function plugin_uninstall()
{
  pwg_query('DELETE FROM '.CONFIG_TABLE.' WHERE param="ProtectAlbum" LIMIT 1;');
  pwg_query('DROP TABLE `'.PROTECTALBUM_TABLE.'`;');
}
?>