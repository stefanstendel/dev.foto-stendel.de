{$MESSAGES}
<form method=post>
<fieldset>
<legend>
  {'PROTECTALBUM_Msg_Prompt'|translate:$CATEGORY_NAME}
</legend>
  <label for="protectalbum_password"><strong>{$QUESTION|translate:$CATEGORY_NAME}</strong></label>
  <input type="password" id="protectalbum_password" name="protectalbum_password"><br/>
  <br />
  <input type="submit" id="protectalbum_login" name="protectalbum_login" value="{'PROTECTALBUM_Login'|@translate}">
</fieldset>
</form>
