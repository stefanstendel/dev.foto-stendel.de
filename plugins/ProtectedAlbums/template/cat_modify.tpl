<fieldset>
  <legend>{'PROTECTALBUM_Title'|@translate} - {'PROTECTALBUM_Version'|@translate}{$PA_VERSION}<br/>{'PROTECTALBUM_SubTitle'|@translate}</legend>
    <p>
     <strong><label for="protectalbum_enabled">{'PROTECTALBUM_Protection'|@translate}</label></strong>
     <br>
     <input type='checkbox' id='protectalbum_enabled' name='protectalbum_enabled' value="ON" {$PA_ENABLED}> 
     {'PROTECTALBUM_Enabled'|@translate}
    </p>
    <p>
      <strong>{'PROTECTALBUM_Prompt'|@translate}</strong><br>
      <input type=text id='protectalbum_question' name='protectalbum_question' value="{$PA_QUESTION}" maxlength=120 style="width:450px">
    </p>
    <p>
      <strong>{'PROTECTALBUM_Password'|@translate}</strong><br>
      <input type=text id='protectalbum_password' name='protectalbum_password' value="{$PA_OLD_PASSWORD}" maxlength=30 style="width:250px">
    </p>
    <p>
     <br>
     <strong><input type='checkbox' id='protectalbum_hideThumb' name='protectalbum_hideThumb' value="ON" {$PA_HIDETHUMB}>
     {'PROTECTALBUM_HideThumb'|@translate}</strong>
     <br>
      <strong>{'PROTECTALBUM_AlternativeThumb'|@translate}</strong><br>
      <input type=text id='protectalbum_alternativeThumb' name='protectalbum_alternativeThumb' value="{$PA_ALTERNATIVETHUMB}" maxlength=255 style="width:450px">
    </p>
    <p style="margin:0">
     <input class="submit" type="submit" name="update_protectalbum_password" value="{'PROTECTALBUM_submit'|@translate}">
  </p>
</fieldset>