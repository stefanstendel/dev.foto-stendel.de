{combine_css path=$PROTECTALBUM_PATH|@cat:"style.css"}
<div class="titrePage">
	<h2>{'PROTECTALBUM_Title'|@translate} {'PROTECTALBUM_Version'|@translate}{$PA_VERSION}<br/>{'PROTECTALBUM_SubTitle'|@translate}</h2>
</div>
<form action="" method="post" class="properties">
		<p class="formButtons">
		  <input type="hidden" name="pwg_token" value="{$PWG_TOKEN}">
		  <strong>{'PROTECTALBUM_AlternativeThumb'|@translate}</strong><br>
		  <input type=text id='alternativeThumb' name='alternativeThumb' value="locked3.jpg" maxlength=255 style="width:450px">
		  <br>
		  <br>
		  <input type="submit" name="Set" value="{'PROTECTALBUM_SetAlternativeThumb'|@translate}" onclick="return confirm('{'Are you sure?'|translate|escape:javascript}')">
		  <br>
		  <br>
		  <input type="submit" name="Reset" value="{'PROTECTALBUM_ReSetAlternativeThumb'|@translate}" onclick="return confirm('{'Are you sure?'|translate|escape:javascript}')">
		</p>
</form>
<fieldset>
  {'PROTECTALBUM_Support_txt'|@translate}
</fieldset>