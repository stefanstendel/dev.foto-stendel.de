<?php
if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
if (!defined('PROTECTALBUM_PATH')) define('PROTECTALBUM_PATH' , PHPWG_PLUGINS_PATH.basename(dirname(__FILE__)).'/');

// debug to delete the sessions: to reactivate the password request
//session_unset();


add_event_handler('loc_begin_index', 'protalbum_begin_index',60);
function protalbum_begin_index()
{
  global $template, $page;
	
	//if admin is logged exit without check
	if (is_autorize_status(ACCESS_ADMINISTRATOR))
	{ 
		return true;
	}
  
  $cat_id = NULL;
  if (isset($page['category']))
  {
    if (is_numeric($page['category']['id']))
      $cat_id = $page['category']['id'];
    else
      trigger_error( 'Bad album id', E_USER_ERROR);

    // If we have authenticated for this category, we are ok
    if (isset($_SESSION['protectalbum_authenticated_'.$cat_id]) && $_SESSION['protectalbum_authenticated_'.$cat_id] === TRUE)
      return;

    // Does this category or any of its parents require a password?
    $query='SELECT uppercats FROM '.CATEGORIES_TABLE.' WHERE id='.$cat_id;
    $result = pwg_query($query);
    $row = pwg_db_fetch_assoc($result);
    $uppercats = $row['uppercats'];

    // Have we already authenticated to the next previous category?
    $ar_uppercats = array_reverse(explode(",",$uppercats));
    foreach ($ar_uppercats as $_cat_id)
    {
      // Walk our way back up the uppercats looking for a password protected category
      // If we find it, check if we're authed, if not, get auth
      $query='SELECT COUNT(*) as cat_count FROM '.PROTECTALBUM_TABLE.' WHERE cat_id = '.$_cat_id.' AND enabled=\'true\'';
      $result = pwg_query($query);
      $row = pwg_db_fetch_assoc($result);
      if ($row['cat_count'] != "0")
      {
        if (isset($_SESSION['protectalbum_authenticated_'.$_cat_id]) && $_SESSION['protectalbum_authenticated_'.$_cat_id] === TRUE) 
		      return;
        $_SESSION['protectalbum_redir'] = $_SERVER['REQUEST_URI'];
        redirect(get_absolute_root_url()."index.php?/password/".$cat_id);
      }
    }
  }
}

add_event_handler('loc_end_index_thumbnails', 'protalbum_loc_end_index_thumbnails');
function protalbum_loc_end_index_thumbnails($tpl_thumbnails_var)
{
		$new_tplvar=array();
    $new_representative=array();
    //limitare questa cosa se si � loggati come admin
    //oppure entrati nell'album con la pwd
    foreach ($tpl_thumbnails_var as $tplvar)
    {
    	//check if it is a protected album and if there is a different thumbnail
    	$query='SELECT enabled, alternativeThumb,category_id FROM '.PROTECTALBUM_TABLE.' INNER JOIN '.IMAGE_CATEGORY_TABLE.' ON category_id = cat_id WHERE image_id = '.$tplvar['id'].' AND enabled=\'true\' AND hideThumb=\'true\'';
    	
    	$result = pwg_query($query);
    	if (pwg_db_num_rows($result) > 0)
    	{
	      $row = pwg_db_fetch_assoc($result);
      	$cat_id=$row['category_id'];
      	if (!isset($_SESSION['protectalbum_authenticated_'.$cat_id]) || !$_SESSION['protectalbum_authenticated_'.$cat_id] === TRUE)
      	{
		      if ($row['alternativeThumb'])
		      {
		      		//se the default or personal alternative image
		      		$tplvar['src_image']->rel_path = ($row['alternativeThumb']?$row['alternativeThumb']:'locked3.jpg');
		      		if (substr($tplvar['src_image']->rel_path,0,1)!=".")
		      		{
		      			$tplvar['src_image']->rel_path = PROTECTALBUM_PATH.$tplvar['src_image']->rel_path;
		      		}
		      }
		    }      	
				$tplvar['representative']=$tplvar;
	    }
	    $new_tplvar[]=$tplvar;     
    }
    return $new_tplvar;
}

add_event_handler('loc_end_index_category_thumbnails', 'protalbum_end_indexcategory_thumbnails');
function protalbum_end_indexcategory_thumbnails($tpl_thumbnails_var)
{
    $new_tplvar=array();
    $new_representative=array();
    
    foreach ($tpl_thumbnails_var as $tplvar)
    {
    	$cat_id=$tplvar['id'];
    	if (!isset($_SESSION['protectalbum_authenticated_'.$cat_id]) || !$_SESSION['protectalbum_authenticated_'.$cat_id] === TRUE)
    	{
	    	//check if it is a protected album and if there is a different thumbnail
	    	$query='SELECT alternativeThumb FROM '.PROTECTALBUM_TABLE.' WHERE cat_id = '.$cat_id.' AND enabled=\'true\' AND hideThumb=\'true\'';
	      $result = pwg_query($query);
	      if (pwg_db_num_rows($result) > 0)
	      {
		      $row = pwg_db_fetch_assoc($result);
		    	$new_representative=$tplvar['representative'];
		      if ($row['alternativeThumb'])
		      {
		      		$new_representative['src_image']->rel_path = ($row['alternativeThumb']?$row['alternativeThumb']:'locked3.jpg');
		      		if (substr($new_representative['src_image']->rel_path,0,1)!=".")
		      		{
		      			$new_representative['src_image']->rel_path = PROTECTALBUM_PATH.$new_representative['src_image']->rel_path;
		      		}
		      }
					$tplvar['representative']=$new_representative;
		    }
	    }
	    $new_tplvar[]=$tplvar;
    }
    return $new_tplvar;
 }

add_event_handler('loc_end_index', 'protalbum_end_index',60);
function protalbum_end_index(){
  global $template, $page,$lang;
  $cat_id = NULL;
  $messages = NULL;
  
  if (substr(@$_SERVER['QUERY_STRING'],0,10) == "/password/")
  {
    if (is_numeric(substr(@$_SERVER['QUERY_STRING'],10)))
      $cat_id = substr(@$_SERVER['QUERY_STRING'],10);
    else
      trigger_error( 'Bad album id', E_USER_ERROR);

    // Get password info
    // We need to get the passwords for the closest level to this category
    // so get the list and read the last row
    $query='SELECT uppercats FROM '.CATEGORIES_TABLE.' WHERE id='.$cat_id;
    $result = pwg_query($query);
    //if (!pwg_db_num_rows($result))
    //  {
    //  	trigger_error( 'Bad album id', E_USER_ERROR);
    //    return false;
    //   }   
 		$row = pwg_db_fetch_assoc($result);
    $uppercats = $row['uppercats'];
    $query='SELECT cat_id, question, password, (SELECT name FROM '.CATEGORIES_TABLE.' WHERE id=cat_id) AS cat_name FROM '.PROTECTALBUM_TABLE.' WHERE cat_id IN ('.$uppercats.') AND enabled=\'true\'';
    $result = pwg_query($query);
    
    while ($row = pwg_db_fetch_assoc($result))
    {
      $prot_cat_id = $row['cat_id'];
      $question = $row['question'];
      $password = $row['password'];
      $cat_name = $row['cat_name'];
    }

    // Did they enter a password?
    if (isset($_POST['protectalbum_password']))
    {
      if ($_POST['protectalbum_password'] == $password)
      {
				// OK, set session and do redir
				$_SESSION['protectalbum_authenticated_'.$prot_cat_id] = TRUE;
				if (!isset($_SESSION['protectalbum_redir']))
				  redirect(get_absolute_root_url()."index.php?/category/".$cat_id);
				else
				  redirect($_SESSION['protectalbum_redir']);
      }
      else
      {
			//if (!isset($page['infos']))
			//  $page['infos'] = array();
      //array_push($page['infos'], "Login Incorrect");
			$messages = "<div class=\"errors\">".$lang['PROTECTALBUM_PasswordErr']	."</div>";
      }
    }

    /*
    $loginform = file_get_contents(PROTECTALBUM_PATH.'template/login_form.tpl')."\n";
    $loginform = str_replace("{QUESTION}", $question, $loginform);
    $loginform = str_replace("{CATEGORY_NAME}", $cat_name, $loginform);
    $loginform = str_replace("{MESSAGES}", $messages, $loginform);
    */
    $template->set_filename('PA_frontend', dirname(__FILE__).'/template/login_form.tpl');
    
    include_once (PROTECTALBUM_PATH.'include/functions.inc.php');
    $plugin =  PROTECTALBUM_Infos(PROTECTALBUM_PATH);
		$version = $plugin['version']; 
		$name = $plugin['name'];
		
    $template->assign('CATEGORY_NAME', $cat_name);
    $template->assign('QUESTION', $question);
    $template->assign('MESSAGES', $messages);
    $template->assign('VERSION', $version);
    
    $template->assign(array( 
    	//'CONTENT_DESCRIPTION' => $loginform,
			'CATEGORIES' => array(),
			'TITLE'=>$name,
    	'CONTENT' => $template->parse('PA_frontend', true),));
		}
}
?>