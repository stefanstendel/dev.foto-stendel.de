<?php
$lang['PROTECTALBUM_Version'] = 'Versiyon: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums:';
$lang['PROTECTALBUM_SubTitle'] = 'Eklenti yapılandırma';
$lang['PROTECTALBUM_submit'] = 'Ayarları Kaydet';
$lang['PROTECTALBUM_save_config'] = 'Ayarlar Kaydedildi';
$lang['PROTECTALBUM_Disabled'] = 'Devre Dışı [varsayılan]';
$lang['PROTECTALBUM_Enabled'] = 'Etkinleştir';
$lang['PROTECTALBUM_Error_Password'] = 'Şifre.';
$lang['PROTECTALBUM_Error_PasswordErr'] = 'Güvenlik: Şifre zorunludur.';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Yönetim';
$lang['PROTECTALBUM_config_tab'] = 'Yapılandırma';
$lang['PROTECTALBUM_Support_txt'] = 'Bu eklentinin resmi desteği şu başlıkta verilmektedir: Piwigo forum:<br/><a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';
$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Albüme Girişi';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Şifre güncellendi ve etkin';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Şifre devre dışı';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>