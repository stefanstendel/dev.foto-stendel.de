<?php
$lang['PROTECTALBUM_Version'] = 'Version: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Plugin-Konfiguration';
$lang['PROTECTALBUM_submit'] = 'Einstellungen übermitteln';
$lang['PROTECTALBUM_save_config'] = 'Einstellungen gespeichert';
$lang['PROTECTALBUM_Disabled'] = 'Deaktivieren (Standard)';
$lang['PROTECTALBUM_Enabled'] = 'Aktivieren';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Passwort:';
$lang['PROTECTALBUM_PasswordErr'] = 'Sicherheit: Passwort wird vorausgesetzt!';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Verwaltung';
$lang['PROTECTALBUM_config_tab'] = 'Konfiguration';
$lang['PROTECTALBUM_Support_txt'] = 'Die offizielle Unterstützung für dieses Plugin ist nur auf diesem Diskussionsforum von Piwigo:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">Englisch-Forum - http://piwigo.org/forum/viewtopic.php?id=</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Passwort aktualisiert und aktiviert';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Passwort deaktiviert';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>