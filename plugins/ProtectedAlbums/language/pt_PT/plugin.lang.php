<?php
$lang['PROTECTALBUM_Version'] = 'Versão: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Configuração da extensão';
$lang['PROTECTALBUM_submit'] = 'Submeter definições';
$lang['PROTECTALBUM_save_config'] = 'Configuração salva';
$lang['PROTECTALBUM_Disabled'] = 'Desativar (padrão)';
$lang['PROTECTALBUM_Enabled'] = 'Ativar';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Senha';
$lang['PROTECTALBUM_PasswordErr'] = 'Segurança: A senha é obrigatória';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Gerenciar';
$lang['PROTECTALBUM_config_tab'] = 'Configuração';
$lang['PROTECTALBUM_Support_txt'] = 'O apoio oficial sobre esta extensão é apenas apenas existe neste tópico do fórum Piwigo: <br>
<a href="http://piwigo.org/forum/viewtopic.php?id=" onclick="window.open(this.href);return false;"> Forum em Inglês - http://piwigo.org/forum/viewtopic.php?id=</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>