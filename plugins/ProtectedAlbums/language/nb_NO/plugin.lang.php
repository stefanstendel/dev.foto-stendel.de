<?php
$lang['PROTECTALBUM_Version'] = 'Versjon: ';
$lang['PROTECTALBUM_Disabled'] = 'Deaktiver (standard)';
$lang['PROTECTALBUM_Enabled'] = 'Aktivert';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Passord';
$lang['PROTECTALBUM_PasswordErr'] = 'Sikkerhet: Passord er obligatorisk!';
$lang['PROTECTALBUM_save_config'] = 'Innstillingene lagret.';
$lang['PROTECTALBUM_submit'] = 'Send inn innstillinger';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Innstillinger for programtillegget';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Administrasjon';
$lang['PROTECTALBUM_config_tab'] = 'Innstillinger';
$lang['PROTECTALBUM_Support_txt'] = 'Den offisielle supporten for dette programtillegget finnes kun p� disse Piwigo forumemnene:<br/>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;"> Engelsk forum - http://piwigo.org /forum/viewtopic.php?id=18751</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>