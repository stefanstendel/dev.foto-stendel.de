<?php
$lang['PROTECTALBUM_Version'] = 'Versión: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Configuración del plugin';
$lang['PROTECTALBUM_submit'] = 'Guardar configuración';
$lang['PROTECTALBUM_save_config'] ='Configuración guardada.';
$lang['PROTECTALBUM_Disabled'] = ' Desactivar (por defecto)';
$lang['PROTECTALBUM_Enabled'] = ' Activar ';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Contraseña:';
$lang['PROTECTALBUM_PasswordErr'] = '¡Seguridad: Contraseña obligatoria!';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Administración';
$lang['PROTECTALBUM_config_tab'] = 'Configuración';
$lang['PROTECTALBUM_Support_txt'] = 'El apoyo oficial sobre este plugin se encuentra solo en el foro de Piwigo:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">Foro Inglés - http://piwigo.org/forum/viewtopic.php?id=18751</a>';


$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>