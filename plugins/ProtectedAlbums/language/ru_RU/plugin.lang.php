<?php
$lang['PROTECTALBUM_Version'] = 'Версия: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums - Защитите Альбомы: ';
$lang['PROTECTALBUM_SubTitle'] = 'Конфигурация плагина';
$lang['PROTECTALBUM_submit'] = 'Отправка настроек';
$lang['PROTECTALBUM_save_config'] = 'Конфигурация сохранена.';
$lang['PROTECTALBUM_Disabled'] = 'Выключено (по умолчанию)';
$lang['PROTECTALBUM_Enabled'] = 'Включено';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Пароль:';
$lang['PROTECTALBUM_PasswordErr'] = 'Служба безопасности: Пароль обязателен!';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Управление';
$lang['PROTECTALBUM_config_tab'] = 'Конфигурация';
$lang['PROTECTALBUM_Support_txt'] = 'Официальная поддрежка этого плагина только на этом форуме Piwigo:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>