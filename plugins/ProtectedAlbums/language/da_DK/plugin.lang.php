<?php
$lang['PROTECTALBUM_Version'] = 'Version: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Opsætning af plugin';
$lang['PROTECTALBUM_submit'] = 'Gem indstillinger';
$lang['PROTECTALBUM_save_config'] = 'Opsætning er gemt.';
$lang['PROTECTALBUM_Disabled'] = ' Deaktiver (standard)';
$lang['PROTECTALBUM_Enabled'] = ' Aktiver';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Adgangskode:';
$lang['PROTECTALBUM_PasswordErr'] = 'Sikkerhed: En adgangskode er påkrævet!';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Håndtering';
$lang['PROTECTALBUM_config_tab'] = 'Opsætning';
$lang['PROTECTALBUM_Support_txt'] = 'Officiel support af denne plugin er kun tilgængelig i disse Piwigo-forumemner:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">Engelsk forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Management';
$lang['PROTECTALBUM_config_tab'] = 'Configuration';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>