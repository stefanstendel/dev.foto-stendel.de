<?php
$lang['PROTECTALBUM_Version'] = 'Versija: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Spraudņa konfigurācija';
$lang['PROTECTALBUM_submit'] = 'Saglabāt iestatījumus';
$lang['PROTECTALBUM_save_config'] ='Konfigurācija saglabāta.';
$lang['PROTECTALBUM_Disabled'] = ' Atspējot (pēc noklusējuma)';
$lang['PROTECTALBUM_Enabled'] = ' Iespējot ';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Parole:';
$lang['PROTECTALBUM_PasswordErr'] = 'Drošība : Parole ir obligāta !';
$lang['PROTECTALBUM_config_tab'] = 'Konfigurācija';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Pārvaldība';
$lang['PROTECTALBUM_Support_txt'] = 'Oficiāls šī spraudņa atbalsts ir pieejams tikai Piwigo forumā:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">Forums angļu valodā - http://piwigo.org/forum/viewtopic.php?id=18751</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>