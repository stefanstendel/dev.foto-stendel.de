<?php
$lang['PROTECTALBUM_Version'] = 'Version: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Plugin configuration';
$lang['PROTECTALBUM_submit'] = 'Submit settings';
$lang['PROTECTALBUM_save_config'] ='Settings saved.';
$lang['PROTECTALBUM_Disabled'] = ' Disable (default)';
$lang['PROTECTALBUM_Enabled'] = ' Enable ';
$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Password';
$lang['PROTECTALBUM_PasswordErr'] = 'Password Error!';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Management';
$lang['PROTECTALBUM_config_tab'] = 'Configuration';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_Support_txt'] = 'The official support on this plugin is only on these Piwigo forum topic:<br/>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>