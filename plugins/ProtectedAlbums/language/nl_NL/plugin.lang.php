<?php
$lang['PROTECTALBUM_Version'] = 'Versie: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums:';
$lang['PROTECTALBUM_SubTitle'] = 'Plugin instellingen';
$lang['PROTECTALBUM_submit'] = 'Bevestig de nieuwe instellingen';
$lang['PROTECTALBUM_save_config'] = 'Instellingen opgeslagen.';
$lang['PROTECTALBUM_Disabled'] = 'Uitschakelen (standaard)';
$lang['PROTECTALBUM_Enabled'] = 'Inschakelen';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Wachtwoord';
$lang['PROTECTALBUM_PasswordErr'] = 'Beveiliging: Wachtwoord is verplicht!';
$lang['PROTECTALBUM_config_tab'] = 'Instellingen';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Beheer';
$lang['PROTECTALBUM_Support_txt'] = 'De offici�le support voor deze plugin kunt u vinden op het volgende Piwigo forum onderwerp:<br/>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">Engels forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';


$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>