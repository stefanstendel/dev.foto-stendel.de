<?php
$lang['PROTECTALBUM_Version'] = 'versão: ';
$lang['PROTECTALBUM_SubTitle'] = 'Configuração do plugin';
$lang['PROTECTALBUM_SubTitle'] = 'Configuração do plugin';
$lang['PROTECTALBUM_submit'] = 'Submeter configurações';
$lang['PROTECTALBUM_save_config'] = 'Configurações salvas';
$lang['PROTECTALBUM_Disabled'] = 'Desabilitar (padrão)';
$lang['PROTECTALBUM_Enabled'] = 'Habilitar';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Senha';
$lang['PROTECTALBUM_PasswordErr'] = 'Segurança: Senha é obrigatório!';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Gerenciamento';
$lang['PROTECTALBUM_config_tab'] = 'Configuração';
$lang['PROTECTALBUM_Support_txt'] = 'O apoio oficial para este plugin é apenas neste tópico no fórum Piwigo:<br/>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a> ';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>