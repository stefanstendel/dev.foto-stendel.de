<?php
$lang['PROTECTALBUM_Version'] = 'Wersja:';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Konfiguracja wtyczki';
$lang['PROTECTALBUM_submit'] = 'Zapisz ustawienia.';
$lang['PROTECTALBUM_save_config'] = 'Ustawienia zapisane.';
$lang['PROTECTALBUM_Disabled'] = 'Wyłącz (domyślnie)';
$lang['PROTECTALBUM_Enabled'] = 'Włącz';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Hasło';
$lang['PROTECTALBUM_PasswordErr'] = 'Bezpieczeństwo: Hasło jest wymagane!';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Wprowadź hasło dla albumu %s';
$lang['PROTECTALBUM_Support_txt'] = 'Oficjalne wsparcie dla tej wtyczki znajduje się tylko w tych tematach na forum Piwigo:<br/>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Management';
$lang['PROTECTALBUM_config_tab'] = 'Configuration';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>