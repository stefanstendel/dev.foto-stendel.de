<?php
$lang['PROTECTALBUM_Version'] = 'Έκδοση: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Διαμόρφωση πρόσθετου';
$lang['PROTECTALBUM_submit'] = 'Υποβολή ρυθμίσεων';
$lang['PROTECTALBUM_save_config'] = 'Η Διαμόρφωση αποθηκεύτηκε.';
$lang['PROTECTALBUM_Disabled'] = 'Απενεργοποίηση (προεπιλογή)';
$lang['PROTECTALBUM_Enabled'] = 'Ενεργοποίηση';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Κωδικός Πρόσβασης';
$lang['PROTECTALBUM_PasswordErr'] = 'Ασφάλεια: Κωδικός Πρόσβασης δεν είναι υποχρεωτικός!';
$lang['PROTECTALBUM_config_tab'] = 'Διαμόρφωση';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Διαχείριση';
$lang['PROTECTALBUM_Support_txt'] = 'Η επίσημη υποστήριξη σε αυτό το πρόσθετο είναι μόνο σε αυτά τα θέμα φόρουμ του Piwigo:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';


$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Κωδικός ενημερωμένο και ενεργοποιημένο';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Κωδικός απενεργοποιηθεί';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>