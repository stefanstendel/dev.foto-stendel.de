<?php
$lang['PROTECTALBUM_Version'] = 'Version: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Configuration du plugin';
$lang['PROTECTALBUM_submit'] = 'Enregistrer les paramètres';
$lang['PROTECTALBUM_save_config'] ='Configuration enregistrée.';
$lang['PROTECTALBUM_Disabled'] = ' Désactiver (valeur par défaut)';
$lang['PROTECTALBUM_Enabled'] = ' Activer ';
$lang['PROTECTALBUM_DefaultQuestion']='Album %s - Entrez le mot de passe:';
$lang['PROTECTALBUM_Password'] = 'Mot de passe:';
$lang['PROTECTALBUM_PasswordErr'] = 'Sécurité: Le Mot de passe est incorrect!';
$lang['PROTECTALBUM_Prompt'] = 'Message à mot de passe';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Entrez le mot de passe de l\'album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Mot de passe requis pour cet album:';
$lang['PROTECTALBUM_Login'] = 'Connectez-vous';
$lang['PROTECTALBUM_Protection'] = 'Protéger par mot de passe:';
$lang['PROTECTALBUM_config_tab'] = 'Configuration';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Gestion';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Mot de passe mis à jour et activé';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Mot de passe désactivé';
$lang['PROTECTALBUM_Support_txt'] = 'Le support officiel sur ce plugin se fait exclusivement sur ce fil du forum FR de Piwigo:<br/>
<a href="http://fr.piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">Forum français - http://fr.piwigo.org/forum/viewtopic.php?id=18751</a>';
$lang['PROTECTALBUM_AlternativeThumb']= 'Fichier image preview alternatif:';
$lang['PROTECTALBUM_HideThumb']= 'Masquer image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Réglez l\'image de remplacement pour tous les albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Réglez l\'aperçu classiques pour tous les albums';
?>