<?php
$lang['PROTECTALBUM_Version'] = 'Versione: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Impostazioni della sicurezza dell\'album tramite password';
$lang['PROTECTALBUM_submit'] = 'Applica';
$lang['PROTECTALBUM_save_config'] = 'Configurazione salvata.';
$lang['PROTECTALBUM_Disabled'] = 'Disabilitata (predefinito)';
$lang['PROTECTALBUM_Enabled'] = 'Abilita';
$lang['PROTECTALBUM_Prompt'] = 'Messaggio di richiesta password';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Per visualizzare l\'album %s è richiesta la relativa password:';
$lang['PROTECTALBUM_Pwd_Required'] = 'È richiesta la password:';
$lang['PROTECTALBUM_DefaultQuestion']='Album %s - Inserire la password:';
$lang['PROTECTALBUM_Password'] = 'Password';
$lang['PROTECTALBUM_PasswordErr'] = 'Password Errata!';
$lang['PROTECTALBUM_Login'] = "Apri l'album";
$lang['PROTECTALBUM_Protection'] = 'Protezione con password:';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Gestione';
$lang['PROTECTALBUM_config_tab'] = 'Configurazione';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password aggiornata e abilitata';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabilitata';
$lang['PROTECTALBUM_Support_txt'] = 'Il supporto ufficiale di questo plugin è solo su questo topic nel forum di Piwigo:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';
$lang['PROTECTALBUM_AlternativeThumb']= 'File dell\'immagine di anteprima alternativa:';
$lang['PROTECTALBUM_HideThumb']= 'Nascondi immagine di anteprima';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Imposta l\'immagine alternativa per tutti gli album';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Imposta l\'anteprima classica per tutti gli album';
?>