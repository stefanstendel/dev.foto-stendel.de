<?php
$lang['PROTECTALBUM_Version'] = 'Verzia: ';
$lang['PROTECTALBUM_Title'] = 'Pravidlá pre heslá';
$lang['PROTECTALBUM_SubTitle'] = 'Nastavenie zásuvného modulu';
$lang['PROTECTALBUM_submit'] = 'Poslať nastavenia';
$lang['PROTECTALBUM_save_config'] = 'Nastavenia uložené.';
$lang['PROTECTALBUM_Disabled'] = 'Zakázať (predvolené)';
$lang['PROTECTALBUM_Enabled'] = 'Povoliť';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Spravovanie';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Heslo:';
$lang['PROTECTALBUM_PasswordErr'] = 'Bezpečnosť: Heslo je povinné!';
$lang['PROTECTALBUM_config_tab'] = 'Nastavenie';
$lang['PROTECTALBUM_Support_txt'] = 'Oficiálna podpora pre tento doplnok je len na týchto Piwigo Forum topic: <br/>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Management';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>