<?php
$lang['PROTECTALBUM_Version'] = 'Verzió: ';
$lang['PROTECTALBUM_Title'] = 'Protected Albums';
$lang['PROTECTALBUM_SubTitle'] = 'Plugin konfiguráció';
$lang['PROTECTALBUM_submit'] = 'Beállítások mentése';
$lang['PROTECTALBUM_save_config'] = 'Konfiguráció mentve.';
$lang['PROTECTALBUM_Disabled'] = ' Kikapcsolva (alapértelmezett)';
$lang['PROTECTALBUM_Enabled'] = ' Bekapcsolva ';
$lang['PROTECTALBUM_DefaultQuestion']='Enter the album password:';
$lang['PROTECTALBUM_Password'] = 'Jelszó:';
$lang['PROTECTALBUM_PasswordErr'] = 'Biztonság : Jelszó kötelező!';
$lang['PROTECTALBUM_Support_txt'] = 'E plugin hivatalos támogatása csak az alábbi Piwigo forum témánál:<br>
<a href="http://piwigo.org/forum/viewtopic.php?id=18751" onclick="window.open(this.href);return false;">English forum - http://piwigo.org/forum/viewtopic.php?id=18751</a>';

$lang['PROTECTALBUM_Prompt'] = 'Prompt';
$lang['PROTECTALBUM_Msg_Prompt'] = 'Enter password for album %s';
$lang['PROTECTALBUM_Pwd_Required'] = 'Password Required for this album:';
$lang['PROTECTALBUM_Login'] = 'Login to Album';
$lang['PROTECTALBUM_Protection'] = 'Password protection:';
$lang['PROTECTALBUM_Users_List_Tab'] = 'Management';
$lang['PROTECTALBUM_config_tab'] = 'Configuration';
$lang['PROTECTALBUM_PasswordEnabled'] = 'Password updated and enabled';
$lang['PROTECTALBUM_PasswordDisabled'] = 'Password disabled';
$lang['PROTECTALBUM_AlternativeThumb']= 'File image preview alternative';
$lang['PROTECTALBUM_HideThumb']= 'Hide image preview';
$lang['PROTECTALBUM_SetAlternativeThumb']= 'Set the alternate image for all albums';
$lang['PROTECTALBUM_ReSetAlternativeThumb']= 'Set the default preview image for all albums';
?>