<?php
if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

add_event_handler('loc_begin_picture', 'protalbum_begin_picture');
function protalbum_begin_picture()
{
  global $template, $page;
	
	//if admin is logged exit without check
	if (is_autorize_status(ACCESS_ADMINISTRATOR)){ 
		return true;
	}
  
  $cat_id = NULL;
  
  if (isset($page['category']))
  {
  	if (is_numeric($page['category']['id']))
      $cat_id = $page['category']['id'];
    // TODO: Add check to picture_pictures data event and look at $data["current"]["url"]=>"picture.php?/100/category/4"
    else
      trigger_error( 'Bad album id', E_USER_ERROR);
  }else{
  	//get category from picture id
  	$query='SELECT category_id FROM '.IMAGE_CATEGORY_TABLE.'
    	WHERE image_id = '.$page['image_id'].' LIMIT 0,1';
      $result = pwg_query($query);
      $row = pwg_db_fetch_assoc($result);
      //die($page['image_id'].IMAGE_CATEGORY_TABLE." - Category:".$row['category_id']);
      $cat_id=$row['category_id'];
  }
  	
	if (!isset($_SESSION['protectalbum_authenticated_'.$cat_id]) 
    || $_SESSION['protectalbum_authenticated_'.$cat_id] !== TRUE)
  {
    $query='SELECT COUNT(*) as cat_count  FROM '.PROTECTALBUM_TABLE
      .' WHERE cat_id='.$cat_id.' AND enabled=\'true\'';
    $result = pwg_query($query);
    $row = pwg_db_fetch_assoc($result);
    if ($row['cat_count'] != "0")
    {
      $_SESSION['protectalbum_redir'] = $_SERVER['REQUEST_URI'];
      redirect(get_absolute_root_url()."index.php?/password/".$cat_id);
    }
  }  
}
?>
