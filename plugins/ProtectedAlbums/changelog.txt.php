<?php
/*
Plugin Name: Protected Albums
** Change log **

-- 0.1.a : Initial release for Piwigo 2.3
					 Added by: unclvito
					 Languages: English [UK] Total : 1
	
-- 0.1.b : Released on 2012-01-27
					 Added plugin url so versions can be checked.

-- 0.1.c : Released on 2012-01-30
					 Fixed compatibility with sub albums. 
					 Now inherits password from parent albums or can be overridden at the sub album.
					 Improved invalid login error message.

-- 0.1.d : Released on 2012-06-03
					 Fixed by mistic100, thank you!
					 Compatible with: 2.3, 2.2
					 fix bug when QUERY_STRING is not in environment.
					 Identified in http://piwigo.org/forum/viewtopic.php?pid=130572#p130572
					 Fixed by mistic100, thank you!

-- 0.2.a : Released on 2012-07-19
					 Compatible with: 2.4
					 Updated to achieve compatibility with Piwigo 2.4. 
					 No longer works properly with Piwigo 2.3.x and earlier!

-- 0.3.a : Not Released
           Updated by leonardo.g
					 Updated to achieve compatibility with Piwigo 2.7.4.

-- 0.4.a : Released on 2014-03-13
					 Updated by leonardo.g
					 Updated to achieve compatibility with Piwigo 2.7.4.
					 Adding access protection of individual photos into protected albums 
					 Add change-log (this file)
					 Add language translation
					 Add it_IT
           Add da_DK, beta version! can you help me? 
           Add de_DE, beta version! can you help me? 
           Add el_GR, beta version! can you help me? 
           Add en_UK, beta version! can you help me? 
           Add es_ES, beta version! can you help me? 
           Add fr_FR, beta version! can you help me? 
           Add hu_HU, beta version! can you help me? 
           Add lv_LV, beta version! can you help me? 
           Add nb_NO, beta version! can you help me? 
           Add nl_NL, beta version! can you help me? 
           Add pl_PL, beta version! can you help me? 
           Add pt_BR, beta version! can you help me? 
           Add pt_PT, beta version! can you help me? 
           Add ru_RU, beta version! can you help me? 
           Add sk_SK, beta version! can you help me? 
           Add tr_TR, beta version! can you help me? 
           Add uk_UA, beta version! can you help me? 
           Add zh_CN, beta version! can you help me? 
           
---- 0.4.b : Released on 2014-03-18           
					 Correct Warning Bug in fr_FR
					 Correct logical Bug in History > Search

---- 0.4.c : Released on 2014-03-18           
					 Changed UPDATE to DELETE when the protection is unchecked 
					 Fixed refresh problerm when you save config
					 Add 2 string in languages translation
					 
					 	
---- 0.5.a : Released on 2016-02-28           
					 Added alternative and optional image for locked albums
					 Added admin tool to set image for all locked albums
					 
---- 0.5.b : Released on 2016-03-04           
					 Added alternative and optional image when show a single photo of locked albums
					 Compatible with: 2.8
					 Correct some problems for V2.8
					 Added some comment in sourcecode
					 	
*/
?>