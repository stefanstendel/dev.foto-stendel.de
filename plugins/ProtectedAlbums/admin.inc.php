<?php
global $user, $lang, $conf, $errors;
global $prefixeTable;
if (!defined('PROTECTALBUM_PATH')) define('PROTECTALBUM_PATH' , PHPWG_PLUGINS_PATH.basename(dirname(__FILE__)).'/');
if (!defined('PROTECTALBUM_TABLE')) define('PROTECTALBUM_TABLE', $prefixeTable.'protectalbum');
	
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', true);

include_once (PHPWG_ROOT_PATH.'admin/include/tabsheet.class.php');
include_once (PHPWG_ROOT_PATH.'/include/constants.php');
include_once (PROTECTALBUM_PATH.'include/functions.inc.php');
  
load_language('plugin.lang', PROTECTALBUM_PATH);

// uncomment this to get the menu under the plugins section
add_event_handler('get_admin_plugin_menu_links', 'protalbum_plugin_admin_menu');
function protalbum_plugin_admin_menu($menu)
{
  array_push(
    $menu,
    array(
      'NAME' => 'Protected Albums',
      'URL' => get_root_url().'admin.php?page=plugin-'.basename(dirname(__FILE__))
      )
    );
  return $menu;
}

add_event_handler('loc_begin_cat_modify', 'ProtectAlbum_cat_modify');
function ProtectAlbum_cat_modify()
{
  global $template, $page, $lang;
  if (!isset( $_GET['cat_id'] ) || !is_numeric( $_GET['cat_id']))
  {
      trigger_error( 'missing cat_id param', E_USER_ERROR);
  }

  $cat_id = $_GET['cat_id'];
  $query='SELECT question, password, enabled, hideThumb, alternativeThumb FROM '.PROTECTALBUM_TABLE.' WHERE cat_id='.$cat_id.'';
  
  $result = pwg_query($query);
  $row = pwg_db_fetch_assoc($result);
  
  $question = $row['question'];
  $password = $row['password'];
  $enabled = $row['enabled'];
	$hideThumb = $row['hideThumb'];
	$alternativeThumb = $row['alternativeThumb'];
	
  if (isset($_POST['update_protectalbum_password']))
  {
    if (isset($_POST['protectalbum_enabled']) && $_POST['protectalbum_enabled'] == 'ON')
    {
			$enabled = "true";
			$question = $_POST['protectalbum_enabled'];
			$question = $_POST['protectalbum_question'];
			$password = $_POST['protectalbum_password'];
			$hideThumb = isset($_POST['protectalbum_hideThumb'])?$_POST['protectalbum_hideThumb']:'';
			$alternativeThumb = isset($_POST['protectalbum_hideThumb'])?$_POST['protectalbum_alternativeThumb']:'';
			
			pwg_query("REPLACE INTO ".PROTECTALBUM_TABLE." (`cat_id`, `enabled`, `question`, `password`, `hideThumb`, `alternativeThumb`) VALUES (".$cat_id.", 'true', '".str_replace("\'", "\\'", $question)."', '".str_replace("\'", "\\'", $password)."','".($hideThumb?'true':'false')."', '".str_replace("\'", "\\'", $alternativeThumb)."')");
      array_push($page['infos'], $lang['PROTECTALBUM_PasswordEnabled']);
    }
    else
    {
    	// if unset the protection delete the row from db
			// ----------------------------------------------
			pwg_query("DELETE FROM ".PROTECTALBUM_TABLE." WHERE cat_id=".$cat_id);
      array_push($page['infos'], $lang['PROTECTALBUM_PasswordDisabled']);
      unset($question);
			unset($enabled);
			unset($password);
			unset($hideThumb);
			unset($alternativeThumb);
    }
  }
	
  if (!isset($question))
    $question = $lang['PROTECTALBUM_DefaultQuestion'];
  if (!isset($password))
    $password = "cippalippa";
  if (!isset($enabled))  
		$enabled = 'false';
	if (!isset($hideThumb))  
		$hideThumb = '';
	if (!isset($alternativeThumb))  
		$alternativeThumb = '';
				
	$question=str_replace("\'", "'", $question);
	
	$plugin =  PROTECTALBUM_Infos(PROTECTALBUM_PATH);
	$version = $plugin['version']; 
	
  $template->set_prefilter('album_properties', 'ProtectAlbum_cat_modify_prefilter');
  $template->assign(array( 
	    'PA_ENABLED' => ($enabled == 'true' ? 'checked' : ''),
	    'PA_QUESTION' => $question,
	    'PA_OLD_PASSWORD' => $password,
	    'PA_HIDETHUMB' => ($hideThumb ? 'checked' : ''),
	    'PA_ALTERNATIVETHUMB' => $alternativeThumb,
	    'PROTECTALBUM_PATH' => PROTECTALBUM_PATH,
	  	'PA_VERSION'    => $version
    ));
}

function ProtectAlbum_cat_modify_prefilter($content, &$smarty)
{
	$search = '<form action="{$F_ACTION}" method="POST" id="links">';
  $replacement = $search . file_get_contents(PROTECTALBUM_PATH.'template/cat_modify.tpl')."\n";
  $search = '</form>';
  $replacement = file_get_contents(PROTECTALBUM_PATH.'template/cat_modify.tpl').$search."\n";
  return str_replace($search, $replacement, $content);
}
?>