<?php
// +-----------------------------------------------------------------------+
// | Manage Properties Photos plugin for Piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

if (!defined('PHPWG_ROOT_PATH'))
    die('Hacking attempt!');
global $template, $conf, $user;
include_once(PHPWG_ROOT_PATH . 'admin/include/tabsheet.class.php');
load_language('plugin.lang', ADD_PROP_PHOTO_PATH);
$my_base_url = get_admin_plugin_menu_link(__FILE__);

// +-----------------------------------------------------------------------+
// | Check Access and exit when user status is not ok                      |
// +-----------------------------------------------------------------------+
check_status(ACCESS_ADMINISTRATOR);

//-------------------------------------------------------- sections definitions
if (!isset($_GET['tab']))
    $page['tab'] = 'define_properties';
else
    $page['tab'] = $_GET['tab'];


if ($page['tab'] != 'iap') {
    $tabsheet = new tabsheet();
    $tabsheet->add('addip', l10n('Property'), ADD_PROP_PHOTO_ADMIN . '-define_properties');
    $tabsheet->select($page['tab']);
    $tabsheet->assign();
} else if ($_GET['tab'] == 'iap') {

    $page['active_menu'] = get_active_menu('photo'); // force oppening "Photos" menu block

    /* Basic checks */
    check_status(ACCESS_ADMINISTRATOR);

    check_input_parameter('image_id', $_GET, false, PATTERN_ID);
    $id_img = $_GET['image_id'];
    $admin_photo_base_url = get_root_url() . 'admin.php?page=photo-' . $_GET['image_id'];

    $page['tab'] = 'iap';

    $tabsheet = new tabsheet();
    $tabsheet->set_id('photo');
    $tabsheet->select('iap');
    $tabsheet->assign();

    $template->assign(
            'gestionD', array(
        'A' => 'a'
    ));
    
    $PAED = pwg_db_fetch_assoc(pwg_query("SELECT state FROM " . PLUGINS_TABLE . " WHERE id = 'ExtendedDescription';"));
    if($PAED['state'] == 'active'){
        add_event_handler('AP_render_content', 'get_user_language_desc');
                $template->assign('useED',1);
    }else{
        $template->assign('useED',0);
    }

    $tab_add_info_one_photo = tab_add_info_by_photo($_GET['image_id']);
    if (pwg_db_num_rows($tab_add_info_one_photo)) {
        while ($info_photos = pwg_db_fetch_assoc($tab_add_info_one_photo)) {

            $d = data_info_photos($id_img, $info_photos['id_prop_pho']);
            $row = pwg_db_fetch_assoc($d);

            $items = array(
                'IDPHO' => $_GET['image_id'],
                'IDINFOPHO' => $info_photos['id_prop_pho'],
                'AIPWORDING' => trigger_change('AP_render_content',$info_photos['wording']),
                'AIPDATA' => $row['data'],
            );

            $template->append('info_photos', $items);
        }
    }

    if (isset($_POST['submitaddinfoimg'])) {
        foreach ($_POST['data'] AS $id_prop_pho => $data) {
            $q = 'SELECT 1 FROM ' . ADD_PROP_PHOTO_DATA_TABLE . ' WHERE id_img=' . $id_img . ' AND id_prop_pho=' . $id_prop_pho;
            $test = pwg_query($q);
            $row = pwg_db_fetch_assoc($test);
            if (count($row) > 0) {
                if ($data != '') {
                    $query = 'UPDATE ' . $prefixeTable . 'add_properties_photos_data SET data="' . $data . '" WHERE id_img=' . $id_img . ' AND id_prop_pho=' . $id_prop_pho;
                    pwg_query($query);
                } else {
                    $query = 'DELETE FROM ' . $prefixeTable . 'add_properties_photos_data WHERE id_img=' . $id_img . ' AND id_prop_pho=' . $id_prop_pho;
                    pwg_query($query);
                }
            } else if ($data != '') {
                $query = 'INSERT ' . $prefixeTable . 'add_properties_photos_data(id_img,id_prop_pho,data) VALUES (' . $id_img . ',' . $id_prop_pho . ',"' . $data . '");';
                pwg_query($query);
            }
            
        }
        $redirect_url = ADD_PROP_PHOTO_ADMIN . '-iap&amp;image_id=' . $id_img;
        $_SESSION['page_infos'] = array(l10n('Properties update'));
        redirect($redirect_url);
    }
}

switch ($page['tab']) {
    case 'define_properties':
        $admin_base_url = ADD_PROP_PHOTO_ADMIN . '-define_properties';
        $template->assign(
                'addinfotemplate', array(
            'addinfo' => l10n('addinfo'),
       ));
        
    $PAED = pwg_db_fetch_assoc(pwg_query("SELECT state FROM " . PLUGINS_TABLE . " WHERE id = 'ExtendedDescription';"));
    if($PAED['state'] == 'active'){
        add_event_handler('AP_render_content', 'get_user_language_desc');
                $template->assign('useED',1);
    }else{
        $template->assign('useED',0);
    }
        
        $admin_base_url = ADD_PROP_PHOTO_ADMIN . '-define_properties';
        $tab_info_photos = tab_info_photos();

        if (pwg_db_num_rows($tab_info_photos)) {
            while ($info_photos = pwg_db_fetch_assoc($tab_info_photos)) {
                if($info_photos['id_prop_pho']==1){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Author'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==2){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Created on'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==3){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Posted on'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==4){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Dimensions'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==5){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('File'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==6){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Filesize'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==7){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Tags'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==8){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Albums'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==9){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Visits'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==10){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Average'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else if($info_photos['id_prop_pho']==11){
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => l10n('Who can see this photo?'),
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }else{
                    $items = array(
                        'IDINFOPHO' => $info_photos['id_prop_pho'],
                        'AIPWORDING' => trigger_change('AP_render_content',$info_photos['wording']),
                        'AIPWORDING2' => $info_photos['wording'],
                        'AIPORDER' => $info_photos['orderprop'],
                        'AIPACTIVE' => $info_photos['active'],
                        'AIPEDIT' => $info_photos['edit'],
                        'U_DELETE' => $admin_base_url . '&amp;delete=' . $info_photos['id_prop_pho'],
                        'U_HIDE' => $admin_base_url . '&amp;hide=' . $info_photos['id_prop_pho'],
                        'U_SHOW' => $admin_base_url . '&amp;show=' . $info_photos['id_prop_pho'],
                    );
                }        
                $template->append('info_photos', $items);
            }
        }
        
        if (isset($_POST['submitManualOrderInfo'])){
            
            asort($_POST['infoOrd'], SORT_NUMERIC);
            
            $data = array();
            foreach ($_POST['infoOrd'] as $id =>$val){
            
            $data[] = array('id_prop_pho' => $id, 'orderprop' => $val+1);
            }
            $fields = array('primary' => array('id_prop_pho'), 'update' => array('orderprop'));
            mass_updates(ADD_PROP_PHOTO_TABLE, $fields, $data);

          $page['infos'][] = l10n('Properties manual order was saved');
          redirect($admin_base_url);
        }

        if (isset($_POST['submitaddAIP'])) {
            if (!isset($_POST['inseractive'])) {
                $_POST['inseractive'] = 0;
            }
            if ($_POST['invisibleID'] == 0) {
                $result = pwg_query('SELECT MAX(orderprop) FROM '. ADD_PROP_PHOTO_TABLE );
                $row = pwg_db_fetch_assoc($result);
                $or = ($row['MAX(orderprop)'] + 1);

                $q = '
                INSERT INTO ' . $prefixeTable . 'add_properties_photos(wording,orderprop,active,edit)VALUES ("' . $_POST['inserwording'] . '","' . $or . '","' . $_POST['inseractive'] . '",1);';
                pwg_query($q);
                $_SESSION['page_infos'] = array(l10n('Property photo add'));
            } else {
                $q = '
                UPDATE ' . $prefixeTable . 'add_properties_photos'
                        . ' set wording ="' . $_POST['inserwording'] . '" '
                        . ' ,active=' . $_POST['inseractive']
                        . ' WHERE id_prop_pho=' . $_POST['invisibleID'] . ';';
                pwg_query($q);
                $_SESSION['page_infos'] = array(l10n('Property photo update'));
            }
            redirect($admin_base_url);
        }

        if (isset($_GET['delete'])) {
            check_input_parameter('delete', $_GET, false, PATTERN_ID);
            $query = 'DELETE FROM ' . ADD_PROP_PHOTO_TABLE . ' WHERE id_prop_pho = ' . $_GET['delete'] . ';';
            pwg_query($query);
            $query = 'DELETE FROM ' . ADD_PROP_PHOTO_DATA_TABLE . ' WHERE id_prop_pho = ' . $_GET['delete'] . ';';
            pwg_query($query);

            $_SESSION['page_infos'] = array(l10n('Property delete'));
            redirect($admin_base_url);
        }
        
        if (isset($_GET['hide'])) {
            check_input_parameter('hide', $_GET, false, PATTERN_ID);
            $query = 'UPDATE ' . ADD_PROP_PHOTO_TABLE . ' SET active = 1 WHERE id_prop_pho=' . $_GET['hide'] . ';';
            pwg_query($query);
        }
        
        if (isset($_GET['show'])) {
            check_input_parameter('show', $_GET, false, PATTERN_ID);
            $query = 'UPDATE ' . ADD_PROP_PHOTO_TABLE . ' SET active = 0 WHERE id_prop_pho=' . $_GET['show'] . ';';
            pwg_query($query);
        }

        break;
 }


$template->set_filenames(array('plugin_admin_content' => dirname(__FILE__) . '/admin/admin.tpl'));
$template->assign_var_from_handle('ADMIN_CONTENT', 'plugin_admin_content');
?>