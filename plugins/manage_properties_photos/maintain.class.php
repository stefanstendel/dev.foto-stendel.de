<?php
// +-----------------------------------------------------------------------+
// | Manage Properties Photos plugin for Piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

defined('PHPWG_ROOT_PATH') or die('Hacking attempt!');

class manage_properties_photos_maintain extends PluginMaintain
{
  private $installed = false;

  function __construct($plugin_id){
    parent::__construct($plugin_id);
  }

  function install($plugin_version, &$errors=array()){
       global $prefixeTable, $conf;

if (!defined('ADD_PROP_PHOTO_TABLE')) define('ADD_PROP_PHOTO_TABLE', $prefixeTable.'add_properties_photos');
	$query = "CREATE TABLE IF NOT EXISTS ". ADD_PROP_PHOTO_TABLE ." (
id_prop_pho SMALLINT(5) UNSIGNED NOT NULL auto_increment,
wording VARCHAR(255) NOT NULL ,
orderprop SMALLINT(5) UNSIGNED NOT NULL ,
active SMALLINT(5) UNSIGNED NOT NULL ,
edit SMALLINT(5) UNSIGNED NOT NULL ,
PRIMARY KEY (id_prop_pho))DEFAULT CHARSET=utf8;";
	$result = pwg_query($query);

if (!defined('ADD_PROP_PHOTO_DATA_TABLE')) define('ADD_PROP_PHOTO_DATA_TABLE', $prefixeTable.'add_properties_photos_data');
      	$query = "CREATE TABLE IF NOT EXISTS ". ADD_PROP_PHOTO_DATA_TABLE ." (
id_img SMALLINT(5) UNSIGNED NOT NULL ,
id_prop_pho SMALLINT(5) UNSIGNED NOT NULL ,
data VARCHAR(255) NOT NULL ,
PRIMARY KEY (id_img,id_prop_pho))DEFAULT CHARSET=utf8;";
	$result = pwg_query($query);
  $activ=unserialize($conf['picture_informations']);
  if($activ['author']==true){$activauteur=0;}else{$activauteur=1;}
  if($activ['created_on']==true){$activco=0;}else{$activco=1;}
  if($activ['posted_on']==true){$activpo=0;}else{$activpo=1;}
  if($activ['dimensions']==true){$activdim=0;}else{$activdim=1;}
  if($activ['file']==true){$activfile=0;}else{$activfile=1;}
  if($activ['filesize']==true){$activfilesize=0;}else{$activfilesize=1;}
  if($activ['tags']==true){$activtags=0;}else{$activtags=1;}
  if($activ['categories']==true){$activcategories=0;}else{$activcategories=1;}
  if($activ['visits']==true){$activvisits=0;}else{$activvisits=1;}
  if($activ['rating_score']==true){$activrs=0;}else{$activrs=1;}
  if($activ['privacy_level']==true){$activpl=0;}else{$activpl=1;}
  $q = 'INSERT INTO ' . $prefixeTable . 'add_properties_photos(id_prop_pho,wording,orderprop,active,edit)VALUES 
	(1,"author",1,'.$activauteur.',0),
	(2,"Created on",2,'.$activco.',0),
	(3,"Posted on",3,'.$activpo.',0),
	(4,"Dimensions",4,'.$activdim.',0),
	(5,"File",5,'.$activfile.',0),
	(6,"Filesize",6,'.$activfilesize.',0),
	(7,"Tags",7,'.$activtags.',0),
	(8,"Albums",8,'.$activcategories.',0),
	(9,"Visits",9,'.$activvisits.',0),
	(10,"Average",10,'.$activrs.',0),
	(11,"Who can see this photo?",11,'.$activpl.',0)
	;';
  pwg_query($q); 
    
  }

  function activate($plugin_version, &$errors=array()){
  
  }

  function update($old_version, $new_version, &$errors=array()){
       global $prefixeTable;
    if (!defined('ADD_PROP_PHOTO_TABLE')) define('ADD_PROP_PHOTO_TABLE', $prefixeTable.'add_properties_photos');
    $desc = pwg_db_fetch_assoc(pwg_query("SELECT wording FROM " . ADD_PROP_PHOTO_TABLE . " WHERE wording = 'Description' LIMIT 1;"));
     if ($desc == NULL){
        $order=pwg_db_fetch_assoc(pwg_query('SELECT orderprop FROM ' . ADD_PROP_PHOTO_TABLE . ' ORDER BY orderprop DESC LIMIT 1;'));	
	$order=$order['orderprop']+1;
    $q = 'INSERT INTO ' . $prefixeTable . 'add_properties_photos(wording,orderprop,active,edit)VALUES ("Description",'.$order.',1,0);';
    pwg_query($q);
     }
      
  }
  
  function deactivate(){
  }

  function uninstall(){
    $q = 'DROP TABLE ' . $prefixeTable . 'add_properties_photos;';
    pwg_query($q);
    $q = 'DROP TABLE ' . $prefixeTable . 'add_properties_photos_data;';
    pwg_query($q);
  }
}
?>
