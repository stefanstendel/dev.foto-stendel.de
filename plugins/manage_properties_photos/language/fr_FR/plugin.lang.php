<?php
// +-----------------------------------------------------------------------+
// | Manage Properties Photos plugin for Piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Property photo update'] = 'Mise à jour la propriété de la photo';
$lang['Wording'] = 'Libellé';
$lang['Create new Property photo'] = 'Créer une nouvelle propriété à la photo';
$lang['Edit Property photo'] = 'Modifier la propriété d\'une photo';
$lang['Manage properties photos'] = 'Gérer les propriétés des photos';
$lang['Properties List'] = 'Liste des propriétés';
$lang['Properties additionals'] = 'Propriétés supplémentaires';
$lang['Properties manual order was saved'] = 'L\'ordre manuel des propriétés a été sauvegardé';
$lang['Properties update'] = 'Mise à jour des propriétés';
$lang['Property'] = 'Propriété';
$lang['Property delete'] = 'Supprimer la propriété';
$lang['Property photo add'] = 'Ajouter propriété à la photo';
$lang['Change photos properties'] = 'Changer les propriétés des photos';
$lang['delete data this property'] = 'supprimer des données de la propriété';
$lang['Choose a property'] = 'Choisir une propriété';