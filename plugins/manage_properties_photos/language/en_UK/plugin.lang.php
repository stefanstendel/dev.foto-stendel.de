<?php
// +-----------------------------------------------------------------------+
// | Manage Properties Photos plugin for Piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
//admin
$lang['Manage properties photos'] = 'Manage properties photos';
$lang['Properties additionals'] = 'Properties additionals';
$lang['Property'] = 'Property';
$lang['Properties update'] = 'Properties update';
$lang['Properties manual order was saved'] = 'Properties manual order was saved';
$lang['Property photo add'] = 'Property photo add';
$lang['Property photo update'] = 'Property photo update';
$lang['Property delete'] = 'Property delete';
$lang['Create new Property photo'] = 'Create new Property photo';
$lang['Edit Property photo'] = 'Edit Property photo';
$lang['Wording'] = 'Wording';
$lang['Properties List'] = 'Properties List';
$lang['Properties additionals'] = 'Properties additionals';
$lang['Change photos properties'] = 'Change photos properties';
$lang['delete data this property'] = 'delete data this property';
$lang['Choose a property'] = 'Choose a property';
