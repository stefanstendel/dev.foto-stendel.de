<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Create new Property photo'] = 'Lege ein neues Foto-Etikett an';
$lang['Edit Property photo'] = 'Bearbeite ein Foto-Etikett';
$lang['Manage properties photos'] = 'Verwalte Foto-Etiketten';
$lang['Properties List'] = 'Etiketten-Liste';
$lang['Properties additionals'] = 'Zusätzliche Etiketten ';
$lang['Properties manual order was saved'] = 'Manuelle Sortierung der Etiketten wurde gespeichert';
$lang['Properties update'] = 'Etiketten Aktualisierung';
$lang['Property'] = 'Etikett';
$lang['Property delete'] = 'Etikett löschen';
$lang['Property photo add'] = 'Foto-Etikett hinzufügen';
$lang['Property photo update'] = 'Foto-Etikett aktualisieren';
$lang['Wording'] = 'Bezeichnung';
$lang['Change photos properties'] = 'Fotoeigenschaften ändern';
$lang['Choose a property'] = 'Eigenschaft wählen';
$lang['delete data this property'] = 'Daten dieser Eigenschaft löschen';