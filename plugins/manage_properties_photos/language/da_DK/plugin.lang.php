<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Create new Property photo'] = 'Opret ny fotoegenskab';
$lang['Edit Property photo'] = 'Rediger fotoegenskab';
$lang['Manage properties photos'] = 'Håndtering fotoegenskaber';
$lang['Properties List'] = 'Egenskabsliste';
$lang['Properties additionals'] = 'Egenskabstilføjelser';
$lang['Properties manual order was saved'] = 'Manuel egenskabsrækkefølge gemt';
$lang['Properties update'] = 'Opdater egenskaber';
$lang['Property'] = 'Egenskab';
$lang['Property delete'] = 'Slet egenskab';
$lang['Property photo add'] = 'Tilføj fotoegenskab';
$lang['Property photo update'] = 'Opdatering fotoegenskab';
$lang['Wording'] = 'Tekst';
$lang['Change photos properties'] = 'Ændring af fotoegenskaber';
$lang['Choose a property'] = 'Vælg en egenskab';
$lang['delete data this property'] = 'slet denne egenskabs data';