<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Properties List'] = 'Īpašību saraksts';
$lang['Wording'] = 'Formulējums';
$lang['Property photo update'] = 'Pazīmes foto atjaunināšana';
$lang['Property photo add'] = 'Pazīmes foto pievienošana';
$lang['Property delete'] = 'Pazīmes dzēšana';
$lang['Property'] = 'Pazīme (Property)';
$lang['Properties update'] = 'Pazīmju atjaunināšana';
$lang['Properties manual order was saved'] = 'Pazīmju manuālā secība ir saglabāta';
$lang['Properties additionals'] = 'Pazīmju papildinājumi';
$lang['Manage properties photos'] = 'Pārvaldīt pazīmju foto';
$lang['Edit Property photo'] = 'Rediģēt pazīmes foto';
$lang['Create new Property photo'] = 'Izveidot jaunu pazīmes (property) foto';