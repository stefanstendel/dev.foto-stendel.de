<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Property'] = 'Egenskap';
$lang['Property delete'] = 'Radera egenskap';
$lang['Property photo add'] = 'Foto-egenskap tillagd';
$lang['Property photo update'] = 'Foto-egenskap uppdaterad';
$lang['Wording'] = 'Formulering';
$lang['Properties update'] = 'Uppdatering av egenskaper';
$lang['Properties manual order was saved'] = 'Manuell sortering av egenskaper sparad';
$lang['Properties additionals'] = 'Ytterligare egenskaper';
$lang['Properties List'] = 'Egenskapslista';
$lang['Manage properties photos'] = 'Hantera foto-egenskaper';
$lang['Edit Property photo'] = 'Redigera foto-egenskap';
$lang['Create new Property photo'] = 'Skapa en ny foto-egenskap';
$lang['delete data this property'] = 'radera denna egenskap';
$lang['Choose a property'] = 'Välj en egenskap';
$lang['Change photos properties'] = 'Ändra bild-egenskaper';