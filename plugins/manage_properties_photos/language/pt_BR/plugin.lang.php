<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Create new Property photo'] = 'Criar nova Propriedade da foto';
$lang['Edit Property photo'] = 'Editar Propriedade da foto';
$lang['Manage properties photos'] = 'Gerenciar propriedades da foto';
$lang['Properties List'] = 'Lista de Propriedades';
$lang['Properties additionals'] = 'Propriedades Adicionais';
$lang['Properties manual order was saved'] = 'Ordem manual de proprieades foi salva';
$lang['Properties update'] = 'Atualizar propriedades';
$lang['Property'] = 'Propriedade';
$lang['Property delete'] = 'Eliminar propriedade';
$lang['Property photo add'] = 'Adicionar propriedades da foto';
$lang['Property photo update'] = 'Atualizar propriedades da foto';
$lang['Wording'] = 'Redação';
$lang['Change photos properties'] = 'Mudar propriedades das fotos';
$lang['You need to confirm'] = 'Você precisa confirmar';