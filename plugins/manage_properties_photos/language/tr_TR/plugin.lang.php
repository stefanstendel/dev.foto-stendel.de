<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Properties update'] = 'Özellikleri güncelle';
$lang['Edit Property photo'] = 'Resim özelliklerini düzenle';
$lang['Properties List'] = 'Özellikler listesi';
$lang['Property'] = 'Özellik';
$lang['Property delete'] = 'Özellik sil';
$lang['Create new Property photo'] = 'Fotonun yeni bir özellik oluştur';
$lang['Manage properties photos'] = 'Fotonun özelliklerini yönet';
$lang['Properties manual order was saved'] = 'Özelliklerin el ile sıralaması kaydettildi';
$lang['Property photo add'] = 'Fotonun yeni bir özellik ekle';
$lang['Property photo update'] = 'Fotonun özellikleri güncellendi';
$lang['Properties additionals'] = 'Ek özellikler';
$lang['Wording'] = 'Metin';