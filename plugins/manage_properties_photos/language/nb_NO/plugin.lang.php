<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Create new Property photo'] = 'Lag nye egenskaper for bilde';
$lang['Edit Property photo'] = 'Rediger bilde egenskaper';
$lang['Manage properties photos'] = 'Behandle egenskaper for bilder';
$lang['Properties List'] = 'Liste over egenskaper';
$lang['Properties additionals'] = 'Tillegsegenskaper';
$lang['Properties manual order was saved'] = 'Liste over egenskaper ble lagret';
$lang['Properties update'] = 'Egenskaper oppdatert';
$lang['Property'] = 'Egenskaper';
$lang['Property delete'] = 'Egenskaper slettet';
$lang['Property photo add'] = 'Legg til egenskaper for bilde';
$lang['Property photo update'] = 'Egenskaper for bilde oppdatert';
$lang['Wording'] = 'Ordlyden';
$lang['Change photos properties'] = 'Endre bilde egenskaper';
$lang['Choose a property'] = 'Velg en egenskap';
$lang['delete data this property'] = 'Slett denne egenskapen';