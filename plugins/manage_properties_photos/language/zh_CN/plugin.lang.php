<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Wording'] = '用词（Libellé, Wording）';
$lang['Create new Property photo'] = '为图片创建新属性';
$lang['Edit Property photo'] = '编辑图片属性';
$lang['Manage properties photos'] = '管理图片属性';
$lang['Properties List'] = '属性一览';
$lang['Properties additionals'] = '附加属性';
$lang['Properties manual order was saved'] = '属性手动排序已保存';
$lang['Properties update'] = '更新属性';
$lang['Property'] = '属性';
$lang['Property delete'] = '删除属性';
$lang['Property photo add'] = '为图片添加属性';
$lang['Property photo update'] = '从图片更新属性';