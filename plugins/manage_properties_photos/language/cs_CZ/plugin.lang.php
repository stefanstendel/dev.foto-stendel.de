<?php
//admin
$lang['Manage properties photos'] = 'Správce vlastností fotografií';
$lang['Properties additionals'] = 'Další vlastnosti';
$lang['Property'] = 'Vlastnost';
$lang['Properties update'] = 'Vlastnosti aktualizovány';
$lang['Properties manual order was saved'] = 'Manuální výběr vlastností byl uložen';
$lang['Property photo add'] = 'Přidat vlastnost fotky';
$lang['Property photo update'] = 'Aktualizovat vlastnost fotky';
$lang['Property delete'] = 'Odstranit vlastnost';
$lang['Create new Property photo'] = 'Vytvořit novou vlastnot fotky';
$lang['Edit Property photo'] = 'Editace vlastnosti fotky';
$lang['Wording'] = 'Formulace';
$lang['Properties List'] = 'Seznam vlastností';
$lang['Properties additionals'] = 'Další vlastnosti';
