<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Property photo update'] = 'Proprietà foto aggiornata';
$lang['Properties manual order was saved'] = 'L\'ordine manuale proprietà è stato salvato';
$lang['Property photo add'] = 'Proprietà foto aggiunta';
$lang['Property delete'] = 'Rimuovere proprietà';
$lang['Properties update'] = 'Proprietà aggiornate';
$lang['Properties List'] = 'Lista proprietà';
$lang['Manage properties photos'] = 'Gestisci proprietà delle foto';
$lang['Edit Property photo'] = 'Modificare proprietà della foto';
$lang['Create new Property photo'] = 'Crea nuova proprietà per la foto';
$lang['Properties additionals'] = 'Proprietà aggiuntive';
$lang['Property'] = 'Proprietà';
$lang['Wording'] = 'Formulazione';
$lang['Change photos properties'] = 'Modificare le proprietà delle foto';
$lang['You need to confirm'] = 'È necessario confermare';