<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Create new Property photo'] = 'Ustvari novo lastnost fotografije';
$lang['Edit Property photo'] = 'Uredi lastnosti fotografije';
$lang['Manage properties photos'] = 'Upravljaj lastnosti fotografij';
$lang['Properties List'] = 'Seznam lastnosti';
$lang['Properties additionals'] = 'Dodatne lastnosti';
$lang['Properties manual order was saved'] = 'Ročna razporeditev lastnosti je bila shranjena';
$lang['Properties update'] = 'Posodobi lastnosti';
$lang['Property'] = 'Lastnosti';
$lang['Property delete'] = 'Izbriši lastnost';
$lang['Property photo add'] = 'Dodaj lastnost';
$lang['Property photo update'] = 'Posodobi lastnost fotografije';
$lang['Wording'] = 'Besedilo';
$lang['Change photos properties'] = 'Spremeni lastnosti slik';
$lang['Choose a property'] = 'Izberi lastnost';
$lang['delete data this property'] = 'izbriši podatke te lastnosti';