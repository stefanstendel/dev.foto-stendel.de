<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Create new Property photo'] = 'Crear nuevo proyecto foto';
$lang['Edit Property photo'] = 'Editar las propriedades de la foto';
$lang['Manage properties photos'] = 'Gestionar las propriedades de la foto';
$lang['Properties List'] = 'Lista de propriedades';
$lang['Properties additionals'] = 'Propriedades adicionales';
$lang['Properties manual order was saved'] = 'Propriedades manual de pedido guardadas';
$lang['Properties update'] = 'Actualización de las propriedades';
$lang['Property'] = 'Propriedad';
$lang['Property delete'] = 'Propriedad borrada';
$lang['Property photo add'] = 'Propriedad foto añadida';
$lang['Property photo update'] = 'Propriedad foto actualizada';
$lang['Wording'] = 'Redacción';