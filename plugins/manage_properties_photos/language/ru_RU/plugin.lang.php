<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Property photo update'] = 'Обновить описание изображения';
$lang['Property photo add'] = 'Добавить описание изображения';
$lang['Property delete'] = 'Описание удалено';
$lang['Property'] = 'Описание';
$lang['Properties update'] = 'Все поля обновлены';
$lang['Properties manual order was saved'] = 'Ручной порядок расположения описаний сохранен';
$lang['Properties additionals'] = 'Дополнительные поля описания';
$lang['Create new Property photo'] = 'Создать новое описание изображения';
$lang['Properties List'] = 'Перечень описаний';
$lang['Manage properties photos'] = 'Управление описаниями изображений';
$lang['Edit Property photo'] = 'Редактировать описание изображения';
$lang['Wording'] = 'Название описания';
$lang['Change photos properties'] = 'Изменение свойств изображений';
$lang['delete data this property'] = 'удаление данных этого свойства';
$lang['Choose a property'] = 'Выбор свойства';