<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Properties manual order was saved'] = 'Manuele rangschikking van de eigenschappen werden bewaard';
$lang['Properties additionals'] = 'Eigenschap toevoegingen';
$lang['Properties List'] = 'Eigenschappen lijst';
$lang['Wording'] = 'Bewoordingen';
$lang['Property delete'] = 'Verwijder eigenschap';
$lang['Property'] = 'Eigenschap';
$lang['Properties update'] = 'Bijwerken eigenschappen';
$lang['Manage properties photos'] = 'Beheer foto eigenschappen';
$lang['Edit Property photo'] = 'Wijzig foto eigenschap';
$lang['Create new Property photo'] = 'Maak nieuwe foto eigenschap';
$lang['Property photo add'] = 'Toevoeging van foto-eigenschappen';
$lang['Property photo update'] = 'Update van foto-eigenschappen';
$lang['Change photos properties'] = 'Wijzig eigenschappen van foto\'s';
$lang['Choose a property'] = 'Kies een eigenschap';
$lang['delete data this property'] = 'verwijder de data van deze eigenschap';