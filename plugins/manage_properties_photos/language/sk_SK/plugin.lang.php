<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Wording'] = 'Formulácia';
$lang['Property photo update'] = 'Aktualizovať fotku objektu';
$lang['Property photo add'] = 'Pridať fotku objektu';
$lang['Property delete'] = 'Vymazaný objekt';
$lang['Property'] = 'Objekty';
$lang['Properties update'] = 'Aktualizovať vlastnosti';
$lang['Properties manual order was saved'] = 'Vlastnosti manuálneho zoradenia boli uložené';
$lang['Properties additionals'] = 'Ďalšie vlastnosti';
$lang['Manage properties photos'] = 'Spravovať vlastnosti fotky';
$lang['Properties List'] = 'Zoznam vlastností';
$lang['Edit Property photo'] = 'Upraviť fotografiu objektu';
$lang['Create new Property photo'] = 'Vytvoriť novú fotografiu objektu';
$lang['delete data this property'] = 'vymazať údaje tejto vlastnosti';
$lang['Choose a property'] = 'Vyberte si vlastnosť';
$lang['Change photos properties'] = 'Zmeniť vlastnosti fotiek';