<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Property photo add'] = 'Προσθήκη Φωτογραφίας ακινήτων';
$lang['Wording'] = 'Η διατύπωση';
$lang['Property photo update'] = 'Ενημέρωση Φωτογραφίας Ακινήτου';
$lang['Property delete'] = 'Διαγραφή Ακινήτου';
$lang['Property'] = 'Ακίνητα';
$lang['Properties update'] = 'Ενημέρωση Ακίνητων';
$lang['Properties manual order was saved'] = 'Η μη-αυτόματη παραγγελία Ακίνητου αποθηκεύτηκε';
$lang['Properties additionals'] = 'Διάφορα πρόσθετα για τα ακίνητα';
$lang['Properties List'] = 'Λίστα Ακινήτων ';
$lang['Manage properties photos'] = 'Διαχείριση φωτογραφιων ακινήτων';
$lang['Edit Property photo'] = 'Επεξεργασία Φωτογραφίας Ακινήτου';
$lang['Create new Property photo'] = 'Δημιουργία νέας φωτογραφίας Ακίνητου';
$lang['Change photos properties'] = 'Αλλαγή ιδιοτήτων φωτογραφιών';
$lang['delete data this property'] = 'Διαγράψετε τα δεδομένα αυτής της ιδιότητας';
$lang['Choose a property'] = 'Επιλέξτε μια ιδιότητα';