<?php
/*
Plugin Name: Manage Properties Photos
Version: 2.8.b
Description: Add properties on photo page and organize this
Plugin URI: http://piwigo.org/ext/extension_view.php?eid=783
Author: ddtddt
Author URI: http://temmii.com/piwigo/
*/

// +-----------------------------------------------------------------------+
// | Manage Properties Photos plugin for Piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+


if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

global $prefixeTable;

define('ADD_PROP_PHOTO_DIR' , basename(dirname(__FILE__)));
define('ADD_PROP_PHOTO_PATH' , PHPWG_PLUGINS_PATH . ADD_PROP_PHOTO_DIR . '/');
if (!defined('ADD_PROP_PHOTO_TABLE')) define('ADD_PROP_PHOTO_TABLE', $prefixeTable.'add_properties_photos');
if (!defined('ADD_PROP_PHOTO_DATA_TABLE')) define('ADD_PROP_PHOTO_DATA_TABLE', $prefixeTable.'add_properties_photos_data');
define('ADD_PROP_PHOTO_ADMIN',get_root_url().'admin.php?page=plugin-'.ADD_PROP_PHOTO_DIR);

include_once(ADD_PROP_PHOTO_PATH . 'include/function.aip.inc.php');

add_event_handler('loading_lang', 'manage_properties_photos_loading_lang');	  
function manage_properties_photos_loading_lang(){
  load_language('plugin.lang', ADD_PROP_PHOTO_PATH);
}

 // Plugin on picture page
if (script_basename() == 'picture'){
  include_once(dirname(__FILE__).'/initpicture.php');
}

  // Plugin for admin
if (script_basename() == 'admin'){
  include_once(dirname(__FILE__).'/initadmin.php');
}

?>