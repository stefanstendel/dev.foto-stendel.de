<?php
// +-----------------------------------------------------------------------+
// | Manage Properties Photos plugin for Piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

//Ajout du prefiltre
add_event_handler('loc_begin_picture', 'add_info_photo_pre', 05);

function add_info_photo_pre() {
    global $template;
    $template->set_prefilter('picture', 'add_info_photo_preT');
}

function add_info_photo_preT($content, &$smarty) {

    global $conf;
  
   $search = '/(<dl id="standard" class="imageInfoTable">).*({if isset\(\$metadata\)})/is';
   
   $repla='<dl id="standard" class="imageInfoTable">{strip}
{foreach from=$add_info_photos item=addinfophotos}
    {if $addinfophotos.AIPID == 1 and isset($INFO_AUTHOR)}
    	<div id="Author" class="imageInfo">
		<dt>{\'Author\'|@translate}</dt>
		<dd>{$INFO_AUTHOR}</dd>
	</div>
    {else if $addinfophotos.AIPID == 2 and isset($INFO_CREATION_DATE)}
        <div id="datecreate" class="imageInfo">
		<dt>{\'Created on\'|@translate}</dt>
		<dd>{$INFO_CREATION_DATE}</dd>
	</div>
    {else if $addinfophotos.AIPID == 3 and isset($INFO_POSTED_DATE)}
	<div id="datepost" class="imageInfo">
		<dt>{\'Posted on\'|@translate}</dt>
		<dd>{$INFO_POSTED_DATE}</dd>
	</div>
    {else if $addinfophotos.AIPID == 4 and isset($INFO_DIMENSIONS)}
	<div id="Dimensions" class="imageInfo">
		<dt>{\'Dimensions\'|@translate}</dt>
		<dd>{$INFO_DIMENSIONS}</dd>
	</div>
    {else if $addinfophotos.AIPID == 5}
	<div id="File" class="imageInfo">
		<dt>{\'File\'|@translate}</dt>
		<dd>{$INFO_FILE}</dd>
	</div>
    {else if $addinfophotos.AIPID == 6 and isset($INFO_FILESIZE)}
	<div id="Filesize" class="imageInfo">
		<dt>{\'Filesize\'|@translate}</dt>
		<dd>{$INFO_FILESIZE}</dd>
	</div>
    {else if $addinfophotos.AIPID == 7 and isset($related_tags)}
    	<div id="Tags" class="imageInfo">
        <dt>{\'Tags\'|@translate}</dt>
		<dd>
		{foreach from=$related_tags item=tag name=tag_loop}{if !$smarty.foreach.tag_loop.first}, {/if}<a href="{$tag.URL}">{$tag.name}</a>{/foreach}
		</dd>
	</div>
    {else if $addinfophotos.AIPID == 8 and isset($related_categories)}
	<div id="Categories" class="imageInfo">
            <dt>{\'Albums\'|@translate}</dt>
            <dd>
                <ul>
                    {foreach from=$related_categories item=cat}
                        <li>{$cat}</li>
                    {/foreach}
                </ul>
            </dd>
	</div>
    {else if $addinfophotos.AIPID == 9}
	<div id="Visits" class="imageInfo">
		<dt>{\'Visits\'|@translate}</dt>
		<dd>{$INFO_VISITS}</dd>
	</div>
    {else if $addinfophotos.AIPID == 10 and isset($rate_summary)}
	<div id="Average" class="imageInfo">
		<dt>{\'Rating score\'|@translate}</dt>
		<dd>
		{if $rate_summary.count}
			<span id="ratingScore">{$rate_summary.score}</span> <span id="ratingCount">({$rate_summary.count|@translate_dec:\'%d rate\':\'%d rates\'})</span>
		{else}
			<span id="ratingScore">{\'no rate\'|@translate}</span> <span id="ratingCount"></span>
		{/if}
		</dd>
	</div>
        {if isset($rating)}
	<div id="rating" class="imageInfo">
		<dt>
			<span id="updateRate">{if isset($rating.USER_RATE)}{\'Update your rating\'|@translate}{else}{\'Rate this photo\'|@translate}{/if}</span>
		</dt>
		<dd>
			<form action="{$rating.F_ACTION}" method="post" id="rateForm" style="margin:0;">
			<div>
			{foreach from=$rating.marks item=mark name=rate_loop}
			{if isset($rating.USER_RATE) && $mark==$rating.USER_RATE}
				<input type="button" name="rate" value="{$mark}" class="rateButtonSelected" title="{$mark}">
			{else}
				<input type="submit" name="rate" value="{$mark}" class="rateButton" title="{$mark}">
			{/if}
			{/foreach}
			{strip}{combine_script id=\'core.scripts\' load=\'async\' path=\'themes/default/js/scripts.js\'}
			{combine_script id=\'rating\' load=\'async\' require=\'core.scripts\' path=\'themes/default/js/rating.js\'}
			{footer_script}
				var _pwgRatingAutoQueue = _pwgRatingAutoQueue||[];
				_pwgRatingAutoQueue.push( {ldelim}rootUrl: \'{$ROOT_URL}\', image_id: {$current.id},
					onSuccess : function(rating) {ldelim}
						var e = document.getElementById("updateRate");
						if (e) e.innerHTML = "{\'Update your rating\'|@translate|@escape:\'javascript\'}";
						e = document.getElementById("ratingScore");
						if (e) e.innerHTML = rating.score;
						e = document.getElementById("ratingCount");
						if (e) {ldelim}
							if (rating.count == 1) {ldelim}
								e.innerHTML = "({\'%d rate\'|@translate|@escape:\'javascript\'})".replace( "%d", rating.count);
							} else {ldelim}
								e.innerHTML = "({\'%d rates\'|@translate|@escape:\'javascript\'})".replace( "%d", rating.count);
              }
						{rdelim}
					{rdelim}{rdelim} );
			{/footer_script}
			{/strip}
			</div>
			</form>
		</dd>
	</div>
        {/if}
    {else if $addinfophotos.AIPID == 11 and $display_info.privacy_level and isset($available_permission_levels)}
	<div id="Privacy" class="imageInfo">
		<dt>{\'Who can see this photo?\'|@translate}</dt>
		<dd>
			<div>
				<a id="privacyLevelLink" href>{$available_permission_levels[$current.level]}</a>
			</div>
{combine_script id=\'core.scripts\' load=\'async\' path=\'themes/default/js/scripts.js\'}
{footer_script require=\'jquery\'}{strip}
function setPrivacyLevel(id, level){
(new PwgWS(\'{$ROOT_URL}\')).callService(
	"pwg.images.setPrivacyLevel", { image_id:id, level:level},
	{
		method: "POST",
		onFailure: function(num, text) { alert(num + " " + text); },
		onSuccess: function(result) {
			  jQuery(\'#privacyLevelBox .switchCheck\').css(\'visibility\',\'hidden\');
				jQuery(\'#switchLevel\'+level).prev(\'.switchCheck\').css(\'visibility\',\'visible\');
				jQuery(\'#privacyLevelLink\').text(jQuery(\'#switchLevel\'+level).text());
		}
	}
	);
}
(SwitchBox=window.SwitchBox||[]).push("#privacyLevelLink", "#privacyLevelBox");
{/strip}{/footer_script}
			<div id="privacyLevelBox" class="switchBox" style="display:none">
				{foreach from=$available_permission_levels item=label key=level}
					<span class="switchCheck"{if $level != $current.level} style="visibility:hidden"{/if}>&#x2714; </span>
					<a id="switchLevel{$level}" href="javascript:setPrivacyLevel({$current.id},{$level})">{$label}</a><br>
				{/foreach}
			</div>
		</dd>
	</div>
    {else if $addinfophotos.AIPWORDING == \'Description\' and isset($COMMENT_IMG)}
	<div id="Description" class="imageInfo">
            <dt>{\'Description\'|@translate}</dt>
            <dd>
                {$COMMENT_IMG}
            </dd>
	</div>
        {footer_script}
            jQuery(document).ready(function(){
              jQuery(".imageComment").hide();
            });
        {/footer_script}
    {else if $addinfophotos.AIPDATA}
        <div id="add_info" class="imageInfo">
          <dt class="label">{$addinfophotos.AIPWORDING}</dt>
          <dd class="value">{$addinfophotos.AIPDATA}</dd>
        </div>
    {/if}
{/foreach}
{/strip}
</dl>
{if isset($metadata)}
';
     
    return preg_replace($search, $repla , $content);

}

add_event_handler('loc_begin_picture', 'add_InfoT');

function add_InfoT() {
    global $conf, $page, $template;

    if (!empty($page['image_id'])) {
        $PAED = pwg_db_fetch_assoc(pwg_query("SELECT state FROM " . PLUGINS_TABLE . " WHERE id = 'ExtendedDescription';"));
        if ($PAED['state'] == 'active')
            add_event_handler('AP_render_content', 'get_user_language_desc');

        $tab_add_info_one_photo = tab_add_info_by_photo_show();

        if (pwg_db_num_rows($tab_add_info_one_photo)) {
            while ($info_photos = pwg_db_fetch_assoc($tab_add_info_one_photo)) {

                $d = data_info_photos($page['image_id'], $info_photos['id_prop_pho']);
                $row = pwg_db_fetch_assoc($d);

                $items = array(
                    'AIPID' => $info_photos['id_prop_pho'],
                    'AIPORDER' => $info_photos['orderprop'],
                    'AIPWORDING' => trigger_change('AP_render_content', $info_photos['wording']),
                    'AIPDATA' => trigger_change('AP_render_content', $row['data']),
                );

                $template->append('add_info_photos', $items);
            }
        }

        $template->assign(
                array(
                    'A' => 'a'
        ));
    }
}

?>