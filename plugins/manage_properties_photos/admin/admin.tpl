{combine_script id='jquery.ui.sortable' require='jquery.ui' load='footer' path='themes/default/js/ui/minified/jquery.ui.sortable.min.js'}
{combine_script id='core.scripts' load='async' path='themes/default/js/scripts.js'}
{footer_script}
jQuery(document).ready(function(){
  jQuery(".drag_button").show();
  jQuery(".categoryLi").css("cursor","move");
  jQuery(".categoryUl").sortable({
    axis: "y",
    opacity: 0.8,
    update : function() {
      jQuery("#manualOrderInfo").show();
    }
  });

  jQuery("#infoOrdering").submit(function(){
    ar = jQuery('.categoryUl').sortable('toArray');
    for(i=0;i < ar.length ;i++) {
      iord = ar[i].split('iord_');
      document.getElementsByName('infoOrd[' + iord[1] + ']')[0].value = i;
    }
  });

  jQuery("#cancelManualOrderInfo").click(function(){
    jQuery(".categoryUl").sortable("cancel");
    jQuery("#manualOrderInfo").hide();
  });
  
  jQuery('.categoryLi').mouseover(function(){
    jQuery(this).children('span').show();
  });
  jQuery('.categoryLi').mouseout(function(){
    jQuery(this).children('span').hide();
  });
  
  jQuery('#aip_sumit').click(function(){
    jQuery("#add_info_edit").show();
    jQuery("#leg_add").show();
    jQuery("#leg_edit").hide();
    jQuery('#aip_add').empty();
    jQuery('#aip_hide').attr('checked', false);
    jQuery('#hideid').val(0);
    jQuery("textarea[name=inserwording]").focus();
  });
  
  jQuery("#addinfoClose").click(function(){
    jQuery("#add_info_edit").hide();
  });
  
  jQuery('.edit_libinfo').click(function(){
    var id_prop_photo=$(this).data('id');
    var lib=$(this).data('lib');
    var hide=$(this).data('hide');
    jQuery("#add_info_edit").show();
    jQuery("#leg_add").hide();
    jQuery("#leg_edit").show();
    jQuery('#hideid').val(id_prop_photo);
    jQuery('#aip_add').text(lib);
        if(hide==0){
            jQuery('#aip_hide').prop('checked', false);
        }else{
            jQuery('#aip_hide').prop('checked', true);
        }
    jQuery("textarea[name=inserwording]").focus();
  });
  jQuery('.pphide').click(function(){
    var id= $(this).data('id');
    var link= $(this).data('link2');
    $.ajax({
        method: 'POST',
        url: link,
        success: function(Datalc,textStatus,jqXHR) {
          jQuery('#pphide'+id).hide();
          jQuery('#ppshow'+id).show();
          jQuery('#iord_'+id).css("opacity","0.4");
        }
      });
    });
  jQuery('.ppshow').click(function(){
    var id= $(this).data('id');
    var link= $(this).data('link2');
    $.ajax({
        method: 'POST',
        url: link,
        success: function(Datalc,textStatus,jqXHR) {
          jQuery('#pphide'+id).show();
          jQuery('#ppshow'+id).hide();
          jQuery('#iord_'+id).css("opacity","1");
         }
      });
    });
});
{/footer_script}
{html_style}
.mouse:hover{
    cursor:pointer;
}
{/html_style}


<div class="titrePage">
  <h2>{'Manage properties photos'|@translate}</h2>
</div>
{if isset ($addinfotemplate)}
        <p class="showCreateAlbum">
            <a href="#" id="aip_sumit" >{'Create new Property photo'|@translate} </a>
        </p>
    <div id="add_info_edit" style="display: none;">
        <form method="post" >
            <fieldset>
                <legend><span id="leg_add">{'Create new Property photo'|@translate}</span><span id="leg_edit">{'Edit Property photo'|@translate}</span></legend>
                <input id="hideid" type="hidden" name="invisibleID" value="{$addinfo_edit2.AIPID}">
                <p class="input">
                    <label for="inserwording">{'Wording'|@translate}</label><br />
                    <textarea {if $useED==1}placeholder="{'Use Extended Description tags...'|@translate}"{/if} style="margin-left:50px" rows="5" cols="50" class="description" name="inserwording" id="aip_add">{$addinfo_edit2.AIPDESC}</textarea>
                    {if $useED==1}
                    <a href="{$ROOT_URL}admin/popuphelp.php?page=extended_desc" onclick="popuphelp(this.href); return false;" title="{'Use Extended Description tags...'|translate}" style="vertical-align: middle; border: 0; margin: 0.5em;"><img src="{$ROOT_URL}{$themeconf.admin_icon_dir}/help.png" class="button" alt="{'Use Extended Description tags...'|translate}'"></a>
                    {/if} 
                </p>
                <p class="input" style="width: 700px;">
                    <label for="inseractive">{'Hide'|@translate}</label>
                    <input id="aip_hide" type="checkbox" name="inseractive" {if {$addinfo_edit2.AIPACTIF}==1}checked{/if} value="1">
                <p class="actionButtons">
                    <input class="submit" name="submitaddAIP" type="submit" value="{'Submit'|@translate}" />
                    <a href="#" id="addinfoClose">{'Cancel'|@translate}</a>
                </p>
            </fieldset>
        </form>
    </div>
    <form id="infoOrdering" method="post" >
        <p id="manualOrderInfo" style="display:none; text-align: left">
          <input class="submit" name="submitManualOrderInfo" type="submit" value="{'Save order'|@translate}">
          {'... or '|@translate} <a href="#" id="cancelManualOrderInfo">{'cancel manual order'|@translate}</a>
        </p>
	<fieldset>
	<legend>{'Properties List'|@translate}</legend>
          <ul class="categoryUl">
            {foreach from=$info_photos item=infophoto}
              <li {if ($infophoto.AIPACTIVE==0)}style="opacity: 1;"{else}style="opacity: 0.4;"{/if}class="categoryLi{if ($infophoto.AIPEDIT==1)} virtual_cat{/if}" id="iord_{$infophoto.IDINFOPHO}">
                <img src="{$themeconf.admin_icon_dir}/cat_move.png" class="drag_button" style="display:none;" alt="{'Drag to re-order'|@translate}" title="{'Drag to re-order'|@translate}">
                {$infophoto.AIPWORDING}
                <input type="hidden" name="infoOrd[{$infophoto.IDINFOPHO}]" value="{$infophoto.AIPORDER}">
                <br />
                <span class="actiononphoto" style="display: none">
                    <span id="pphide{$infophoto.IDINFOPHO}" {if ($infophoto.AIPACTIVE==1)}style="display: none"{/if}class="graphicalCheckbox icon-check-empty mouse pphide" data-id="{$infophoto.IDINFOPHO}" data-link2="{$infophoto.U_HIDE}">{'Hide'|@translate}</span>
                    <span id="ppshow{$infophoto.IDINFOPHO}" {if ($infophoto.AIPACTIVE==0)}style="display: none"{/if}class="graphicalCheckbox icon-check mouse ppshow" data-id="{$infophoto.IDINFOPHO}" data-link2="{$infophoto.U_SHOW}">{'Hide'|@translate}</span>
                    {if ($infophoto.AIPEDIT==1)}
					| <span class="edit_libinfo mouse icon-pencil" data-id="{$infophoto.IDINFOPHO}" data-lib="{$infophoto.AIPWORDING2}" data-hide="{$infophoto.AIPACTIVE}"/>{'Edit'|@translate}</span>
					| <a href="{$infophoto.U_DELETE}" onclick="return confirm('{'Are you sure?'|@translate|@escape:javascript}');"><span class="icon-trash"></span>{'delete'|@translate}</a>
                    {/if}
                </span>
                <br />
              </li>
            {/foreach}
          </ul>
        </fieldset>
    </form>
{/if}
{if isset ($gestionD)}
<div>
    <form method="post" >
	<fieldset>
	<legend>{'Properties additionals'|@translate}</legend>
        <table>
            {foreach from=$info_photos item=infophoto}
            <tr>
                <td style="width: 100px;"><span style="font-weight: bold; text-align: right;" >{$infophoto.AIPWORDING}</span></td>
                <td><input type="text" size="150" maxlength="250" {if $useED==1}placeholder="{'Use Extended Description tags...'|@translate}"{/if} name="data[{$infophoto.IDINFOPHO}]" value="{$infophoto.AIPDATA}" /></td>
            </tr>  
            {/foreach}
            <tr style="text-align: right;">
                <td colspan="2">
                    {if $useED==1}
                        <a href="{$ROOT_URL}admin/popuphelp.php?page=extended_desc" onclick="popuphelp(this.href); return false;" title="{'Use Extended Description tags...'|translate}" style="vertical-align: middle; border: 0; margin: 0.5em;"><img src="{$ROOT_URL}{$themeconf.admin_icon_dir}/help.png" class="button" alt="{'Use Extended Description tags...'|translate}'"></a>
                    {/if}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input class="submit" name="submitaddinfoimg" type="submit" value="{'Save'|@translate}" />
                </td>
           </tr>
           </table>
  	</fieldset>
    </form>
</div>
{/if}