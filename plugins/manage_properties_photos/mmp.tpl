{footer_script}
jQuery(document).ready(function(){
  $("input[name=check_MPP]").change(function(){
    if($(this).is(':checked')){
	  $('#changepro').hide();
    }else{
	  $('#changepro').show();
    }
  }); 
});
{/footer_script}
<span id="persompp">
  {'Choose a property'|@translate}
  <br />
  {html_options name="IDMPP" options=$propertieslist selected=$SELECTEDMPP}
  <br />
  <br />
  <input id="check_MPP" type="checkbox" name="check_MPP"> {'delete data this property'|@translate}<br />
  <div id="changepro">
    <form method="post" >
      <textarea rows="3" cols="100" {if $useED==1}placeholder="{'Use Extended Description tags...'|@translate}"{/if} name="dataglob">{$PLUG_MPP}</textarea>
    </form>
  </div>
</span>