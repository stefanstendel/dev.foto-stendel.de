<?php
/*
Plugin Name: Persönliches Plugin
Version: 1.0
Description: Persönliches Plugin
Plugin URI: http://piwigo.org
Author:
Author URI:
*/

add_event_handler('loc_end_element_set_global', 'bm_gen_custom_derivatives_form');
add_event_handler('element_set_global_action', 'bm_gen_custom_derivatives_action', EVENT_HANDLER_PRIORITY_NEUTRAL, 2);
  
function bm_gen_custom_derivatives_form() {
  global $template;
  $template_add = '';
  foreach (array_keys(ImageStdParams::$custom) as $custom) {
    $template_add = $template_add . '<input type="checkbox" name="custom_sizes[]" value="' . $custom . '">' . $custom . '<br />';
  }
  
  $template->append('element_set_global_plugins_actions', array(
      'ID' => 'GenCustomDerivatives', 
      'NAME' => l10n('Pre-cache custom size derivatives'), 
      'CONTENT' => $template_add,
  ));
}

function bm_gen_custom_derivatives_action($action, $collection) {
  if ($action == 'GenCustomDerivatives'){
    global $page, $user, $conf, $template;
    foreach ($collection as $image_id){
      if (isset($_POST['custom_sizes'])) {
        $http_opts = array('http' => array('header' => 'Referer: ' . get_absolute_root_url() . '\r\n' . 
                                                'Cookie: ' . $_SERVER['HTTP_COOKIE'] . '\r\n'));
        $http_context = stream_context_create($http_opts);
        
        $result = file_get_contents(get_absolute_root_url() .'ws.php?format=json&method=pwg.images.getInfo&image_id=' . $image_id, NULL, $http_context);
        $json_result = json_decode($result);

//var_dump($json_result);
//exit;

        $sq_url = $json_result->result->derivatives->square->url;
echo $sq_url;
exit;        
        foreach ($_POST['custom_sizes'] as $size) {
          $url = str_replace('sq.jpg', 'cu_' . $size . '.jpg', $sq_url);
          file_get_contents($url, NULL, $http_context);
        }
      }
    }
  }
}

