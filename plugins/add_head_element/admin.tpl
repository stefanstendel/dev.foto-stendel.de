{html_style}{literal}
form p {text-align:left;margin:0 0 1.5em 1.5em;line-height:20px;}
{/literal}{/html_style}

<div class="titrePage">
  <h2>{'Add < head > element'|@translate}</h2>
</div>

<form method="post">
  <p>
    <strong>{'ahe_perso'|@translate}</strong><br>
    <textarea rows="5" cols="50" class="description" name="ahe" id="ahe">{$AHEBASE}</textarea>
  </p>

  <p>
    <strong>{'Apply on'|@translate}</strong><br>
    <label><input type="checkbox" name="apply_on_gallery"{if $gallery} checked="checked"{/if}> {'Gallery'|@translate}</label>
    <label><input type="checkbox" name="apply_on_admin"{if $admin} checked="checked"{/if}> {'Administration'|@translate}</label>
  </p>

  <p>
    <input class="submit" type="submit" name="submitahe" value="{'Submit'|@translate}">
  </p>
</form>
