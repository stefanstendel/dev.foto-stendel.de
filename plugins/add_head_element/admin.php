<?php
// +-----------------------------------------------------------------------+
// | Add < head > element plugin for piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2011 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
global $template, $conf, $user;
include_once(PHPWG_ROOT_PATH .'admin/include/tabsheet.class.php');
$my_base_url = get_admin_plugin_menu_link(__FILE__);

// +-----------------------------------------------------------------------+
// | Check Access and exit when user status is not ok                      |
// +-----------------------------------------------------------------------+
check_status(ACCESS_ADMINISTRATOR);

//-------------------------------------------------------- sections definitions

$tabsheet = new tabsheet();
$tabsheet->add('Management', l10n('Management'), '');
$tabsheet->select('Management');
$tabsheet->assign();

if (isset($conf['add_head_element_apply_on']))
{
  $conf['add_head_element_apply_on'] = explode(',', $conf['add_head_element_apply_on']);
}
else
{
  $conf['add_head_element_apply_on'] = array('gallery', 'admin');
}

$template->assign(
  array(
    'AHEBASE' => $conf['add_head_element'],
    'gallery' => in_array('gallery', $conf['add_head_element_apply_on']),
    'admin' => in_array('admin', $conf['add_head_element_apply_on']),
    )
  );

if (isset($_POST['submitahe']))
{
  conf_update_param('add_head_element', $_POST['ahe']);

  $conf['add_head_element_apply_on'] = array();
  
  if (!empty($_POST['apply_on_gallery']))
  {
    $conf['add_head_element_apply_on'][] = 'gallery';
  }
  
  if (!empty($_POST['apply_on_admin']))
  {
    $conf['add_head_element_apply_on'][] = 'admin';
  }

  conf_update_param('add_head_element_apply_on', implode(',', $conf['add_head_element_apply_on']));
  
  $template->assign(
    array(
      'AHEBASE' => stripslashes($_POST['ahe']),
      'gallery' => in_array('gallery', $conf['add_head_element_apply_on']),
      'admin' => in_array('admin', $conf['add_head_element_apply_on']),
      )
    );
}

$template->set_filenames(array('plugin_admin_content' => dirname(__FILE__) . '/admin.tpl')); 
$template->assign_var_from_handle('ADMIN_CONTENT', 'plugin_admin_content');
?>