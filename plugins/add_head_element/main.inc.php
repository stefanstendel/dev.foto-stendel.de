<?php
/*
Plugin Name: Add < head > element
Version: 2.8.a
Description: Add element to tag < head >
Plugin URI: http://piwigo.org/ext/extension_view.php?eid=582
Author: ddtddt
Author URI: http://temmii.com/piwigo/
*/

// +-----------------------------------------------------------------------+
// | Add < head > element plugin for piwigo                            |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2011 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

global $prefixeTable;

define('AHE_DIR' , basename(dirname(__FILE__)));
define('AHE_PATH' , PHPWG_PLUGINS_PATH . AHE_DIR . '/');

add_event_handler('loading_lang', 'Add_head_element_loading_lang');	  
function Add_head_element_loading_lang(){
  load_language('plugin.lang', AHE_PATH);
}

add_event_handler('get_admin_plugin_menu_links', 'AHE_admin_menu');
function AHE_admin_menu($menu)
{
  array_push(
    $menu,
    array(
      'NAME' => l10n('Add < head > element'),
      'URL' => get_root_url().'admin.php?page=plugin-add_head_element'
      )
    );
  
  return $menu;
}


//add in <head>
add_event_handler('loc_begin_page_header', 'AHE1',20 );

function AHE1()
{
  global $template,$conf;

  if (isset($conf['add_head_element_apply_on']))
  {
    if (!is_array($conf['add_head_element_apply_on']))
    {
      $conf['add_head_element_apply_on'] = explode(',', $conf['add_head_element_apply_on']);
    }

    if (defined('IN_ADMIN') and IN_ADMIN)
    {
      // we are in the administration
      if (!in_array('admin', $conf['add_head_element_apply_on']))
      {
        return;
      }
    }
    else
    {
      // we are in the gallery
      if (!in_array('gallery', $conf['add_head_element_apply_on']))
      {
        return;
      }
    }
    
  }
  
	if (!empty($conf['add_head_element']))
	{
    $template->append('head_elements', $conf['add_head_element']);
	}
}
?>