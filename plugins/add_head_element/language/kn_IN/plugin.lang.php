<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Apply on'] = 'ರಂದು ಸಲ್ಲಿಸಿ';
$lang['Gallery'] = 'ಚಿತ್ರಾಂಕಣ';
$lang['Management'] = 'ನಿರ್ವಹಣೆ';
$lang['Add < head > element'] = 'ಪ್ಲಗ್ ಇನ್ ಸೇರಿಸಿ < head > ಅಂಶ';
$lang['ahe_perso'] = '< head >  HTML ವಿಭಾಗದಲ್ಲಿ ಅಂಶಗಳನ್ನು ಸೇರಿಸಿ';
?>